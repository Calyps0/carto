import sys
import imp
import os
import os.path
import zipfile
from cStringIO import StringIO
import math
from random import randint
import numpy
from PIL import Image, ImageEnhance, ImageOps, ImageDraw
import logging
import functools

import util
from c_overviewer import alpha_over

class TextureException(Exception):
    pass


color_map = ["white", "orange", "magenta", "light_blue", "yellow", "lime", "pink", "gray",
             "silver", "cyan", "purple", "blue", "brown", "green", "red", "black"]

##
## Textures object
##
class Textures(object):
    """An object that generates a set of block sprites to use while
    rendering. It accepts a background color, north direction, and
    local textures path.
    """
    def __init__(self, texturepath=None, bgcolor=(26, 26, 26, 0), northdirection=0, customblocks=None):
        self.bgcolor = bgcolor
        self.rotation = northdirection
        self.find_file_local_path = texturepath
        self.customblocks = customblocks

        # not yet configurable
        self.texture_size = 24
        self.texture_dimensions = (self.texture_size, self.texture_size)

        # this is set in in generate()
        self.generated = False

        # see load_image_texture()
        self.texture_cache = {}

        # see save_rendered_block_image()
        self.rendered_block_cache = {}
        self.rendered_block_data = {}

        # once we find a jarfile that contains a texture, we cache the ZipFile object here
        self.jar = None
        self.jarpath = ""

    ##
    ## pickle support
    ##

    def __getstate__(self):
        # we must get rid of the huge image lists, and other images
        attributes = self.__dict__.copy()
        for attr in ['blockmap', 'biome_grass_texture', 'watertexture', 'lavatexture', 'firetexture', 'portaltexture', 'lightcolor', 'grasscolor', 'foliagecolor', 'watercolor', 'texture_cache', 'rendered_block_cache', 'rendered_block_data']:
            try:
                del attributes[attr]
            except KeyError:
                pass
        return attributes
    def __setstate__(self, attrs):
        # regenerate textures, if needed
        for attr, val in attrs.iteritems():
            setattr(self, attr, val)
        self.texture_cache = {}
        self.rendered_block_cache = {}
        self.rendered_block_data = {}
    
        if self.generated:
            self.generate()

    ##
    ## The big one: generate()
    ##

    def generate(self):

        # generate biome grass mask
        self.biome_grass_texture = self.build_block(self.load_image_texture("assets/minecraft/textures/blocks/grass_top.png"), self.load_image_texture("assets/minecraft/textures/blocks/grass_side_overlay.png"))

        # generate the blocks
        global blockmap_generators
        global known_blocks, used_datas
        self.blockmap = [None] * max_blockid * max_data
        updated_blocks = set()

        for (blockid, data), texgen in blockmap_generators.iteritems():
            tex = self.check_block_exist(blockid, data)
            if tex == None:
                updated_blocks.add(blockid)
                if blockid in oriented_blocks:
                    save_rotation = self.rotation
                    for i in range(4):
                        self.rotation = i
                        tex = texgen(self, blockid, data)
                        self.save_rendered_block_image(tex, blockid, data + (i * last_data[blockid]))
                    self.rotation = save_rotation
                else:
                    tex = texgen(self, blockid, data)
                    self.save_rendered_block_image(tex, blockid, data)
            self.blockmap[blockid * max_data + data] = self.generate_texture_tuple(tex)

        self.save_cache(updated_blocks)

        if self.texture_size != 24:
            # rescale biome grass
            self.biome_grass_texture = self.biome_grass_texture.resize(self.texture_dimensions, Image.ANTIALIAS)

            # rescale the rest
            for i, tex in enumerate(blockmap):
                if tex is None:
                    continue
                block = tex[0]
                scaled_block = block.resize(self.texture_dimensions, Image.ANTIALIAS)
                blockmap[i] = self.generate_texture_tuple(scaled_block)

        self.generated = True

    ##
    ## Helpers for opening textures
    ##

    def find_file(self, filename, mode="rb", verbose=False):
        """Searches for the given file and returns an open handle to it.
        This searches the following locations in this order:

        * In the directory textures_path given in the initializer
        * In the resource pack given by textures_path
        * The program dir (same dir as overviewer.py) for extracted textures
        * On Darwin, in /Applications/Minecraft for extracted textures
        * Inside a minecraft client jar. Client jars are searched for in the
          following location depending on platform:

            * On Windows, at %APPDATA%/.minecraft/versions/
            * On Darwin, at
                $HOME/Library/Application Support/minecraft/versions
            * at $HOME/.minecraft/versions/

          Only the latest non-snapshot version >1.6 is used

        * The overviewer_core/data/textures dir

        In all of these, files are searched for in '.', 'anim', 'misc/', and
        'environment/'.

        """
        if verbose: logging.info("Starting search for {0}".format(filename))

        # a list of subdirectories to search for a given file,
        # after the obvious '.'
        search_dirs = ['anim', 'misc', 'environment', 'item', 'item/chests', 'entity', 'entity/chest']
        search_zip_paths = [filename,] + [d + '/' + filename for d in search_dirs]
        def search_dir(base):
            """Search the given base dir for filename, in search_dirs."""
            for path in [os.path.join(base, d, filename) for d in ['',] + search_dirs]:
                if verbose: logging.info('filename: ' + filename + ' ; path: ' + path)
                if os.path.isfile(path):
                    return path

            return None
        if verbose: logging.info('search_zip_paths: ' +  ', '.join(search_zip_paths))

        # A texture path was given on the command line. Search this location
        # for the file first.
        if self.find_file_local_path:
            if os.path.isdir(self.find_file_local_path):
                path = search_dir(self.find_file_local_path)
                if path:
                    if verbose: logging.info("Found %s in '%s'", filename, path)
                    return open(path, mode)
            elif os.path.isfile(self.find_file_local_path):
                # Must be a resource pack. Look for the requested file within
                # it.
                try:
                    pack = zipfile.ZipFile(self.find_file_local_path)
                    for packfilename in search_zip_paths:
                        try:
                            # pack.getinfo() will raise KeyError if the file is
                            # not found.
                            pack.getinfo(packfilename)
                            if verbose: logging.info("Found %s in '%s'", packfilename, self.find_file_local_path)
                            return pack.open(packfilename)
                        except (KeyError, IOError):
                            pass

                        try:
                            # 2nd try with completed path.
                            packfilename = 'assets/minecraft/textures/' + packfilename
                            pack.getinfo(packfilename)
                            if verbose: logging.info("Found %s in '%s'", packfilename, self.find_file_local_path)
                            return pack.open(packfilename)
                        except (KeyError, IOError):
                            pass
                except (zipfile.BadZipfile, IOError):
                    pass

        # If we haven't returned at this point, then the requested file was NOT
        # found in the user-specified texture path or resource pack.
        if verbose: logging.info("Did not find the file in specified texture path")


        # Look in the location of the overviewer executable for the given path
        programdir = util.get_program_path()
        path = search_dir(programdir)
        if path:
            if verbose: logging.info("Found %s in '%s'", filename, path)
            return open(path, mode)

        if sys.platform.startswith("darwin"):
            path = search_dir("/Applications/Minecraft")
            if path:
                if verbose: logging.info("Found %s in '%s'", filename, path)
                return open(path, mode)

        if verbose: logging.info("Did not find the file in overviewer executable directory")
        if verbose: logging.info("Looking for installed minecraft jar files...")

        # we've sucessfully loaded something from here before, so let's quickly try
        # this before searching again
        if self.jar is not None:
            for jarfilename in search_zip_paths:
                try:
                    self.jar.getinfo(jarfilename)
                    if verbose: logging.info("Found (cached) %s in '%s'", jarfilename, self.jarpath)
                    return self.jar.open(jarfilename)
                except (KeyError, IOError), e:
                    pass

        # Find an installed minecraft client jar and look in it for the texture
        # file we need.
        versiondir = ""
        if "APPDATA" in os.environ and sys.platform.startswith("win"):
            versiondir = os.path.join(os.environ['APPDATA'], ".minecraft", "versions")
        elif "HOME" in os.environ:
            # For linux:
            versiondir = os.path.join(os.environ['HOME'], ".minecraft", "versions")
            if not os.path.exists(versiondir) and sys.platform.startswith("darwin"):
                # For Mac:
                versiondir = os.path.join(os.environ['HOME'], "Library",
                    "Application Support", "minecraft", "versions")

        try:
            if verbose: logging.info("Looking in the following directory: \"%s\"" % versiondir)
            versions = os.listdir(versiondir)
            if verbose: logging.info("Found these versions: {0}".format(versions))
        except OSError:
            # Directory doesn't exist? Ignore it. It will find no versions and
            # fall through the checks below to the error at the bottom of the
            # method.
            versions = []

        most_recent_version = [0,0,0]
        for version in versions:
            # Look for the latest non-snapshot that is at least 1.8. This
            # version is only compatible with >=1.8, and we cannot in general
            # tell if a snapshot is more or less recent than a release.

            # Allow two component names such as "1.8" and three component names
            # such as "1.8.1"
            if version.count(".") not in (1,2):
                continue
            try:
                versionparts = [int(x) for x in version.split(".")]
            except ValueError:
                continue

            if versionparts < [1,8]:
                continue

            if versionparts > most_recent_version:
                most_recent_version = versionparts

        if most_recent_version != [0,0,0]:
            if verbose: logging.info("Most recent version >=1.8.0: {0}. Searching it for the file...".format(most_recent_version))

            jarname = ".".join(str(x) for x in most_recent_version)
            jarpath = os.path.join(versiondir, jarname, jarname + ".jar")

            if os.path.isfile(jarpath):
                jar = zipfile.ZipFile(jarpath)
                for jarfilename in search_zip_paths:
                    try:
                        jar.getinfo(jarfilename)
                        if verbose: logging.info("Found %s in '%s'", jarfilename, jarpath)
                        self.jar, self.jarpath = jar, jarpath
                        return jar.open(jarfilename)
                    except (KeyError, IOError), e:
                        pass

            if verbose: logging.info("Did not find file {0} in jar {1}".format(filename, jarpath))
        else:
            if verbose: logging.info("Did not find any non-snapshot minecraft jars >=1.8.0")

        # Last ditch effort: look for the file is stored in with the overviewer
        # installation. We include a few files that aren't included with Minecraft
        # textures. This used to be for things such as water and lava, since
        # they were generated by the game and not stored as images. Nowdays I
        # believe that's not true, but we still have a few files distributed
        # with overviewer.
        if verbose: logging.info("Looking for texture in overviewer_core/data/textures")
        path = search_dir(os.path.join(programdir, "overviewer_core", "data", "textures"))
        if path:
            if verbose: logging.info("Found %s in '%s'", filename, path)
            return open(path, mode)
        elif hasattr(sys, "frozen") or imp.is_frozen("__main__"):
            # windows special case, when the package dir doesn't exist
            path = search_dir(os.path.join(programdir, "textures"))
            if path:
                if verbose: logging.info("Found %s in '%s'", filename, path)
                return open(path, mode)

        raise TextureException("Could not find the textures while searching for '{0}'. Try specifying the 'texturepath' option in your config file.\nSet it to the path to a Minecraft Resource pack.\nAlternately, install the Minecraft client (which includes textures)\nAlso see <http://docs.overviewer.org/en/latest/running/#installing-the-textures>\n(Remember, this version of Overviewer requires a 1.12-compatible resource pack)\n(Also note that I won't automatically use snapshots; you'll have to use the texturepath option to use a snapshot jar)".format(filename))

    def load_image_texture(self, filename):
        # Textures may be animated or in a different resolution than 16x16.
        # This method will always return a 16x16 image

        img = self.load_image(filename)

        w,h = img.size
        if w != h:
            img = img.crop((0,0,w,w))
        if w != 16:
            img = img.resize((16, 16), Image.ANTIALIAS)

        self.texture_cache[filename] = img
        return img

    def load_image(self, filename):
        """Returns an image object"""

        if filename in self.texture_cache:
            return self.texture_cache[filename]

        fileobj = self.find_file(filename)
        buffer = StringIO(fileobj.read())
        img = Image.open(buffer).convert("RGBA")
        self.texture_cache[filename] = img
        return img



    def load_water(self):
        """Special-case function for loading water, handles
        MCPatcher-compliant custom animated water."""
        watertexture = getattr(self, "watertexture", None)
        if watertexture:
            return watertexture
        try:
            # try the MCPatcher case first
            watertexture = self.load_image("custom_water_still.png")
            watertexture = watertexture.crop((0, 0, watertexture.size[0], watertexture.size[0]))
        except TextureException:
            watertexture = self.load_image_texture("assets/minecraft/textures/blocks/water_still.png")
        self.watertexture = watertexture
        return watertexture

    def load_lava(self):
        """Special-case function for loading lava, handles
        MCPatcher-compliant custom animated lava."""
        lavatexture = getattr(self, "lavatexture", None)
        if lavatexture:
            return lavatexture
        try:
            # try the MCPatcher lava first, in case it's present
            lavatexture = self.load_image("custom_lava_still.png")
            lavatexture = lavatexture.crop((0, 0, lavatexture.size[0], lavatexture.size[0]))
        except TextureException:
            lavatexture = self.load_image_texture("assets/minecraft/textures/blocks/lava_still.png")
        self.lavatexture = lavatexture
        return lavatexture

    def load_fire(self):
        """Special-case function for loading fire, handles
        MCPatcher-compliant custom animated fire."""
        firetexture = getattr(self, "firetexture", None)
        if firetexture:
            return firetexture
        try:
            # try the MCPatcher case first
            firetextureNS = self.load_image("custom_fire_n_s.png")
            firetextureNS = firetextureNS.crop((0, 0, firetextureNS.size[0], firetextureNS.size[0]))
            firetextureEW = self.load_image("custom_fire_e_w.png")
            firetextureEW = firetextureEW.crop((0, 0, firetextureEW.size[0], firetextureEW.size[0]))
            firetexture = (firetextureNS,firetextureEW)
        except TextureException:
            fireNS = self.load_image_texture("assets/minecraft/textures/blocks/fire_layer_0.png")
            fireEW = self.load_image_texture("assets/minecraft/textures/blocks/fire_layer_1.png")
            firetexture = (fireNS, fireEW)
        self.firetexture = firetexture
        return firetexture

    def load_portal(self):
        """Special-case function for loading portal, handles
        MCPatcher-compliant custom animated portal."""
        portaltexture = getattr(self, "portaltexture", None)
        if portaltexture:
            return portaltexture
        try:
            # try the MCPatcher case first
            portaltexture = self.load_image("custom_portal.png")
            portaltexture = portaltexture.crop((0, 0, portaltexture.size[0], portaltexture.size[1]))
        except TextureException:
            portaltexture = self.load_image_texture("assets/minecraft/textures/blocks/portal.png")
        self.portaltexture = portaltexture
        return portaltexture

    def load_light_color(self):
        """Helper function to load the light color texture."""
        if hasattr(self, "lightcolor"):
            return self.lightcolor
        try:
            lightcolor = list(self.load_image("light_normal.png").getdata())
        except Exception:
            logging.warning("Light color image could not be found.")
            lightcolor = None
        self.lightcolor = lightcolor
        return lightcolor

    def load_grass_color(self):
        """Helper function to load the grass color texture."""
        if not hasattr(self, "grasscolor"):
            self.grasscolor = list(self.load_image("grass.png").getdata())
        return self.grasscolor

    def load_foliage_color(self):
        """Helper function to load the foliage color texture."""
        if not hasattr(self, "foliagecolor"):
            self.foliagecolor = list(self.load_image("foliage.png").getdata())
        return self.foliagecolor

    #I guess "watercolor" is wrong. But I can't correct as my texture pack don't define water color.
    def load_water_color(self):
        """Helper function to load the water color texture."""
        if not hasattr(self, "watercolor"):
            self.watercolor = list(self.load_image("watercolor.png").getdata())
        return self.watercolor

    def _split_terrain(self, terrain):
        """Builds and returns a length 256 array of each 16x16 chunk
        of texture.
        """
        textures = []
        (terrain_width, terrain_height) = terrain.size
        texture_resolution = terrain_width / 16
        for y in xrange(16):
            for x in xrange(16):
                left = x*texture_resolution
                upper = y*texture_resolution
                right = left+texture_resolution
                lower = upper+texture_resolution
                region = terrain.transform(
                          (16, 16),
                          Image.EXTENT,
                          (left,upper,right,lower),
                          Image.BICUBIC)
                textures.append(region)

        return textures

    ##
    ## Image Transformation Functions
    ##

    @staticmethod
    def transform_image_top(img):
        """Takes a PIL image and rotates it left 45 degrees and shrinks the y axis
        by a factor of 2. Returns the resulting image, which will be 24x12 pixels

        """

        # Resize to 17x17, since the diagonal is approximately 24 pixels, a nice
        # even number that can be split in half twice
        img = img.resize((17, 17), Image.ANTIALIAS)

        # Build the Affine transformation matrix for this perspective
        transform = numpy.matrix(numpy.identity(3))
        # Translate up and left, since rotations are about the origin
        transform *= numpy.matrix([[1,0,8.5],[0,1,8.5],[0,0,1]])
        # Rotate 45 degrees
        ratio = math.cos(math.pi/4)
        #transform *= numpy.matrix("[0.707,-0.707,0;0.707,0.707,0;0,0,1]")
        transform *= numpy.matrix([[ratio,-ratio,0],[ratio,ratio,0],[0,0,1]])
        # Translate back down and right
        transform *= numpy.matrix([[1,0,-12],[0,1,-12],[0,0,1]])
        # scale the image down by a factor of 2
        transform *= numpy.matrix("[1,0,0;0,2,0;0,0,1]")

        transform = numpy.array(transform)[:2,:].ravel().tolist()

        newimg = img.transform((24,12), Image.AFFINE, transform)
        return newimg

    @staticmethod
    def transform_image_side(img):
        """Takes an image and shears it for the left side of the cube (reflect for
        the right side)"""

        # Size of the cube side before shear
        img = img.resize((12,12), Image.ANTIALIAS)

        # Apply shear
        transform = numpy.matrix(numpy.identity(3))
        transform *= numpy.matrix("[1,0,0;-0.5,1,0;0,0,1]")

        transform = numpy.array(transform)[:2,:].ravel().tolist()

        newimg = img.transform((12,18), Image.AFFINE, transform)
        return newimg

    @staticmethod
    def transform_image_slope(img):
        """Takes an image and shears it in the shape of a slope going up
        in the -y direction (reflect for +x direction). Used for minetracks"""

        # Take the same size as trasform_image_side
        img = img.resize((12,12), Image.ANTIALIAS)

        # Apply shear
        transform = numpy.matrix(numpy.identity(3))
        transform *= numpy.matrix("[0.75,-0.5,3;0.25,0.5,-3;0,0,1]")
        transform = numpy.array(transform)[:2,:].ravel().tolist()

        newimg = img.transform((24,24), Image.AFFINE, transform)

        return newimg


    @staticmethod
    def transform_image_angle(img, angle):
        """Takes an image an shears it in arbitrary angle with the axis of
        rotation being vertical.

        WARNING! Don't use angle = pi/2 (or multiplies), it will return
        a blank image (or maybe garbage).

        NOTE: angle is in the image not in game, so for the left side of a
        block angle = 30 degree.
        """

        # Take the same size as trasform_image_side
        img = img.resize((12,12), Image.ANTIALIAS)

        # some values
        cos_angle = math.cos(angle)
        sin_angle = math.sin(angle)

        # function_x and function_y are used to keep the result image in the
        # same position, and constant_x and constant_y are the coordinates
        # for the center for angle = 0.
        constant_x = 6.
        constant_y = 6.
        function_x = 6.*(1-cos_angle)
        function_y = -6*sin_angle
        big_term = ( (sin_angle * (function_x + constant_x)) - cos_angle* (function_y + constant_y))/cos_angle

        # The numpy array is not really used, but is helpful to
        # see the matrix used for the transformation.
        transform = numpy.array([[1./cos_angle, 0, -(function_x + constant_x)/cos_angle],
                                 [-sin_angle/(cos_angle), 1., big_term ],
                                 [0, 0, 1.]])

        transform = tuple(transform[0]) + tuple(transform[1])

        newimg = img.transform((24,24), Image.AFFINE, transform)

        return newimg

    def save_rendered_block_image(self, img, blockid, data):
        required_width = (data + 1) * 24
        block_img = None
        if blockid in self.rendered_block_cache:
            block_img = self.rendered_block_cache[blockid]
        else:
            if not os.path.isdir('cache/'):
                os.mkdir('cache', 0755)
            cache_file_path = 'cache/' + str(blockid) + '.png'
            if os.path.isfile(cache_file_path):
                block_img = Image.open(cache_file_path)
            else:
                block_img = Image.new("RGBA", (required_width, 24), self.bgcolor)

        if (block_img.size[0] < required_width):
            new_img = Image.new("RGBA", (required_width, 24), self.bgcolor)
            new_img.paste(block_img, (0, 0))
            block_img = new_img

        if (img != None):
            block_img.paste(img, (data * 24, 0))
    
        self.rendered_block_cache[blockid] = block_img

    def save_cache(self, blockids):
        for blockid in blockids:
            if blockid in self.rendered_block_cache:
                self.rendered_block_cache[blockid].save('cache/' + str(blockid) + '.png')

    def check_block_exist(self, blockid, data):

        if blockid in oriented_blocks:
            data = data + (self.rotation * last_data[blockid])

        if blockid in self.rendered_block_cache:
            block_img = self.rendered_block_cache[blockid]
        else:
            cache_file_path = 'cache/' + str(blockid) + '.png'
            if os.path.isfile(cache_file_path):
                block_img = Image.open(cache_file_path)
                self.rendered_block_cache[blockid] = block_img
                self.rendered_block_data[blockid] = set(range(block_img.size[0] / 24))
            else:
                self.rendered_block_data[blockid] = set()
                return None

        required_width = (data + 1) * 24
        if (block_img.size[0] < required_width):
            return None

        if not data in self.rendered_block_data[blockid]:
            return None

        self.rendered_block_data[blockid].add(data)
        return block_img.crop((data * 24, 0, (data * 24) + 24, 24))

##
## The other big one: @material and associated framework
##

# global variables to collate information in @material decorators
blockmap_generators = {}

known_blocks = set()
used_datas = set()
max_blockid = 0
max_data = 0

transparent_blocks = set()
solid_blocks = set()
fluid_blocks = set()
nospawn_blocks = set()
nodata_blocks = set()

# the material registration decorator
def material(blockid=[], data=[0], **kwargs):
    # mapping from property name to the set to store them in
    properties = {"transparent" : transparent_blocks, "solid" : solid_blocks, "fluid" : fluid_blocks, "nospawn" : nospawn_blocks, "nodata" : nodata_blocks}

    # make sure blockid and data are iterable
    try:
        iter(blockid)
    except:
        blockid = [blockid,]
    try:
        iter(data)
    except:
        data = [data,]

    def inner_material(func):
        global blockmap_generators
        global max_data, max_blockid

        # create a wrapper function with a known signature
        @functools.wraps(func)
        def func_wrapper(texobj, blockid, data):
            return func(texobj, blockid, data)

        used_datas.update(data)
        if max(data) >= max_data:
            max_data = max(data) + 1

        for block in blockid:
            # set the property sets appropriately
            known_blocks.update([block])
            if block >= max_blockid:
                max_blockid = block + 1
            for prop in properties:
                try:
                    if block in kwargs.get(prop, []):
                        properties[prop].update([block])
                except TypeError:
                    if kwargs.get(prop, False):
                        properties[prop].update([block])

            # populate blockmap_generators with our function
            for d in data:
                blockmap_generators[(block, d)] = func_wrapper

        return func_wrapper
    return inner_material

@material(blockid=[101,102, 160], data=range(256), transparent=True, nospawn=True)
def panes(self, blockid, data):
    # no rotation, uses pseudo data
    if blockid == 101:
        # iron bars
        t = self.load_image_texture("assets/minecraft/textures/blocks/iron_bars.png")
    elif blockid == 160:
        t = self.load_image_texture("assets/minecraft/textures/blocks/glass_%s.png" % color_map[data & 0xf])
    else:
        # glass panes
        t = self.load_image_texture("assets/minecraft/textures/blocks/glass.png")
    left = t.copy()
    right = t.copy()

    # generate the four small pieces of the glass pane
    ImageDraw.Draw(right).rectangle((0,0,7,15),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(left).rectangle((8,0,15,15),outline=(0,0,0,0),fill=(0,0,0,0))

    up_left = self.transform_image_side(left)
    up_right = self.transform_image_side(right).transpose(Image.FLIP_TOP_BOTTOM)
    dw_right = self.transform_image_side(right)
    dw_left = self.transform_image_side(left).transpose(Image.FLIP_TOP_BOTTOM)

    # Create img to compose the texture
    img = Image.new("RGBA", (24,24), self.bgcolor)

    # +x axis points top right direction
    # +y axis points bottom right direction
    # First compose things in the back of the image,
    # then things in the front.

    # the lower 4 bits encode color, the upper 4 encode adjencies
    data = data >> 4

    if (data & 0b0001) == 1 or data == 0:
        alpha_over(img,up_left, (6,3),up_left)    # top left
    if (data & 0b1000) == 8 or data == 0:
        alpha_over(img,up_right, (6,3),up_right)  # top right
    if (data & 0b0010) == 2 or data == 0:
        alpha_over(img,dw_left, (6,3),dw_left)    # bottom left
    if (data & 0b0100) == 4 or data == 0:
        alpha_over(img,dw_right, (6,3),dw_right)  # bottom right

    return img

# melon
block(blockid=103, top_image="assets/minecraft/textures/blocks/melon_top.png", side_image="assets/minecraft/textures/blocks/melon_side.png", solid=True)

# pumpkin and melon stem
# TODO To render it as in game needs from pseudo data and ancil data:
# once fully grown the stem bends to the melon/pumpkin block,
# at the moment only render the growing stem
@material(blockid=[104,105], data=range(8), transparent=True)
def stem(self, blockid, data):
    # the ancildata value indicates how much of the texture
    # is shown.

    # not fully grown stem or no pumpkin/melon touching it,
    # straight up stem
    t = self.load_image_texture("assets/minecraft/textures/blocks/melon_stem_disconnected.png").copy()
    img = Image.new("RGBA", (16,16), self.bgcolor)
    alpha_over(img, t, (0, int(16 - 16*((data + 1)/8.))), t)
    img = self.build_sprite(t)
    if data & 7 == 7:
        # fully grown stem gets brown color!
        # there is a conditional in rendermode-normal.c to not
        # tint the data value 7
        img = self.tint_texture(img, (211,169,116))
    return img


# vines
@material(blockid=106, data=range(16), transparent=True)
def vines(self, blockid, data):
    shifts = 0
    if self.rotation == 1:
        shifts = 1
    elif self.rotation == 2:
        shifts = 2
    elif self.rotation == 3:
        shifts = 3

    for i in range(shifts):
        data = data * 2
        if data & 16:
            data = (data - 16) | 1

    # decode data and prepare textures
    raw_texture = self.load_image_texture("assets/minecraft/textures/blocks/vine.png")
    s = w = n = e = None

    if data & 1: # south
        s = raw_texture
    if data & 2: # west
        w = raw_texture
    if data & 4: # north
        n = raw_texture
    if data & 8: # east
        e = raw_texture

    # texture generation
    img = self.build_full_block(None, n, e, w, s)

    return img

# fence gates
@material(blockid=[107, 183, 184, 185, 186, 187], data=range(8), transparent=True, nospawn=True)
def fence_gate(self, blockid, data):

    # rotation
    opened = False
    if data & 0x4:
        data = data & 0x3
        opened = True
    if self.rotation == 1:
        if data == 0: data = 1
        elif data == 1: data = 2
        elif data == 2: data = 3
        elif data == 3: data = 0
    elif self.rotation == 2:
        if data == 0: data = 2
        elif data == 1: data = 3
        elif data == 2: data = 0
        elif data == 3: data = 1
    elif self.rotation == 3:
        if data == 0: data = 3
        elif data == 1: data = 0
        elif data == 2: data = 1
        elif data == 3: data = 2
    if opened:
        data = data | 0x4

    # create the closed gate side
    if blockid == 107: # Oak
        gate_side = self.load_image_texture("assets/minecraft/textures/blocks/planks_oak.png").copy()
    elif blockid == 183: # Spruce
        gate_side = self.load_image_texture("assets/minecraft/textures/blocks/planks_spruce.png").copy()
    elif blockid == 184: # Birch
        gate_side = self.load_image_texture("assets/minecraft/textures/blocks/planks_birch.png").copy()
    elif blockid == 185: # Jungle
        gate_side = self.load_image_texture("assets/minecraft/textures/blocks/planks_jungle.png").copy()
    elif blockid == 186: # Dark Oak
        gate_side = self.load_image_texture("assets/minecraft/textures/blocks/planks_big_oak.png").copy()
    elif blockid == 187: # Acacia
        gate_side = self.load_image_texture("assets/minecraft/textures/blocks/planks_acacia.png").copy()
    else:
        return None

    gate_side_draw = ImageDraw.Draw(gate_side)
    gate_side_draw.rectangle((7,0,15,0),outline=(0,0,0,0),fill=(0,0,0,0))
    gate_side_draw.rectangle((7,4,9,6),outline=(0,0,0,0),fill=(0,0,0,0))
    gate_side_draw.rectangle((7,10,15,16),outline=(0,0,0,0),fill=(0,0,0,0))
    gate_side_draw.rectangle((0,12,15,16),outline=(0,0,0,0),fill=(0,0,0,0))
    gate_side_draw.rectangle((0,0,4,15),outline=(0,0,0,0),fill=(0,0,0,0))
    gate_side_draw.rectangle((14,0,15,15),outline=(0,0,0,0),fill=(0,0,0,0))

    # darken the sides slightly, as with the fences
    sidealpha = gate_side.split()[3]
    gate_side = ImageEnhance.Brightness(gate_side).enhance(0.9)
    gate_side.putalpha(sidealpha)

    # create the other sides
    mirror_gate_side = self.transform_image_side(gate_side.transpose(Image.FLIP_LEFT_RIGHT))
    gate_side = self.transform_image_side(gate_side)
    gate_other_side = gate_side.transpose(Image.FLIP_LEFT_RIGHT)
    mirror_gate_other_side = mirror_gate_side.transpose(Image.FLIP_LEFT_RIGHT)

    # Create img to compose the fence gate
    img = Image.new("RGBA", (24,24), self.bgcolor)

    if data & 0x4:
        # opened
        data = data & 0x3
        if data == 0:
            alpha_over(img, gate_side, (2,8), gate_side)
            alpha_over(img, gate_side, (13,3), gate_side)
        elif data == 1:
            alpha_over(img, gate_other_side, (-1,3), gate_other_side)
            alpha_over(img, gate_other_side, (10,8), gate_other_side)
        elif data == 2:
            alpha_over(img, mirror_gate_side, (-1,7), mirror_gate_side)
            alpha_over(img, mirror_gate_side, (10,2), mirror_gate_side)
        elif data == 3:
            alpha_over(img, mirror_gate_other_side, (2,1), mirror_gate_other_side)
            alpha_over(img, mirror_gate_other_side, (13,7), mirror_gate_other_side)
    else:
        # closed

        # positions for pasting the fence sides, as with fences
        pos_top_left = (2,3)
        pos_top_right = (10,3)
        pos_bottom_right = (10,7)
        pos_bottom_left = (2,7)

        if data == 0 or data == 2:
            alpha_over(img, gate_other_side, pos_top_right, gate_other_side)
            alpha_over(img, mirror_gate_other_side, pos_bottom_left, mirror_gate_other_side)
        elif data == 1 or data == 3:
            alpha_over(img, gate_side, pos_top_left, gate_side)
            alpha_over(img, mirror_gate_side, pos_bottom_right, mirror_gate_side)

    return img

# mycelium
block(blockid=110, top_image="assets/minecraft/textures/blocks/mycelium_top.png", side_image="assets/minecraft/textures/blocks/mycelium_side.png")

# lilypad
# At the moment of writing this lilypads has no ancil data and their
# orientation depends on their position on the map. So it uses pseudo
# ancildata.
@material(blockid=111, data=range(4), transparent=True)
def lilypad(self, blockid, data):
    t = self.load_image_texture("assets/minecraft/textures/blocks/waterlily.png").copy()
    if data == 0:
        t = t.rotate(180)
    elif data == 1:
        t = t.rotate(270)
    elif data == 2:
        t = t
    elif data == 3:
        t = t.rotate(90)

    return self.build_full_block(None, None, None, None, None, t)

# nether brick
block(blockid=112, top_image="assets/minecraft/textures/blocks/nether_brick.png")

# nether wart
@material(blockid=115, data=range(4), transparent=True)
def nether_wart(self, blockid, data):
    if data == 0: # just come up
        t = self.load_image_texture("assets/minecraft/textures/blocks/nether_wart_stage_0.png")
    elif data in (1, 2):
        t = self.load_image_texture("assets/minecraft/textures/blocks/nether_wart_stage_1.png")
    else: # fully grown
        t = self.load_image_texture("assets/minecraft/textures/blocks/nether_wart_stage_2.png")

    # use the same technic as tall grass
    img = self.build_billboard(t)

    return img

# enchantment table
# TODO there's no book at the moment
@material(blockid=116, transparent=True, nodata=True)
def enchantment_table(self, blockid, data):
    # no book at the moment
    top = self.load_image_texture("assets/minecraft/textures/blocks/enchanting_table_top.png")
    side = self.load_image_texture("assets/minecraft/textures/blocks/enchanting_table_side.png")
    img = self.build_full_block((top, 4), None, None, side, side)

    return img

# brewing stand
# TODO this is a place holder, is a 2d image pasted
@material(blockid=117, data=range(5), transparent=True)
def brewing_stand(self, blockid, data):
    base = self.load_image_texture("assets/minecraft/textures/blocks/brewing_stand_base.png")
    img = self.build_full_block(None, None, None, None, None, base)
    t = self.load_image_texture("assets/minecraft/textures/blocks/brewing_stand.png")
    stand = self.build_billboard(t)
    alpha_over(img,stand,(0,-2))
    return img

# cauldron
@material(blockid=118, data=range(4), transparent=True)
def cauldron(self, blockid, data):
    side = self.load_image_texture("assets/minecraft/textures/blocks/cauldron_side.png")
    top = self.load_image_texture("assets/minecraft/textures/blocks/cauldron_top.png")
    bottom = self.load_image_texture("assets/minecraft/textures/blocks/cauldron_inner.png")
    water = self.transform_image_top(self.load_water())
    if data == 0: # empty
        img = self.build_full_block(top, side, side, side, side)
    if data == 1: # 1/3 filled
        img = self.build_full_block(None , side, side, None, None)
        alpha_over(img, water, (0,8), water)
        img2 = self.build_full_block(top , None, None, side, side)
        alpha_over(img, img2, (0,0), img2)
    if data == 2: # 2/3 filled
        img = self.build_full_block(None , side, side, None, None)
        alpha_over(img, water, (0,4), water)
        img2 = self.build_full_block(top , None, None, side, side)
        alpha_over(img, img2, (0,0), img2)
    if data == 3: # 3/3 filled
        img = self.build_full_block(None , side, side, None, None)
        alpha_over(img, water, (0,0), water)
        img2 = self.build_full_block(top , None, None, side, side)
        alpha_over(img, img2, (0,0), img2)

    return img

# end portal and end_gateway
@material(blockid=[119,209], transparent=True, nodata=True)
def end_portal(self, blockid, data):
    img = Image.new("RGBA", (24,24), self.bgcolor)
    # generate a black texure with white, blue and grey dots resembling stars
    t = Image.new("RGBA", (16,16), (0,0,0,255))
    for color in [(155,155,155,255), (100,255,100,255), (255,255,255,255)]:
        for i in range(6):
            x = randint(0,15)
            y = randint(0,15)
            t.putpixel((x,y),color)
    if blockid == 209: # end_gateway
        return  self.build_block(t, t)
        
    t = self.transform_image_top(t)
    alpha_over(img, t, (0,0), t)

    return img

# end portal frame (data range 8 to get all orientations of filled)
@material(blockid=120, data=range(8), transparent=True)
def end_portal_frame(self, blockid, data):
    # The bottom 2 bits are oritation info but seems there is no
    # graphical difference between orientations
    top = self.load_image_texture("assets/minecraft/textures/blocks/endframe_top.png")
    eye_t = self.load_image_texture("assets/minecraft/textures/blocks/endframe_eye.png")
    side = self.load_image_texture("assets/minecraft/textures/blocks/endframe_side.png")
    img = self.build_full_block((top, 4), None, None, side, side)
    if data & 0x4 == 0x4: # ender eye on it
        # generate the eye
        eye_t = self.load_image_texture("assets/minecraft/textures/blocks/endframe_eye.png").copy()
        eye_t_s = self.load_image_texture("assets/minecraft/textures/blocks/endframe_eye.png").copy()
        # cut out from the texture the side and the top of the eye
        ImageDraw.Draw(eye_t).rectangle((0,0,15,4),outline=(0,0,0,0),fill=(0,0,0,0))
        ImageDraw.Draw(eye_t_s).rectangle((0,4,15,15),outline=(0,0,0,0),fill=(0,0,0,0))
        # trnasform images and paste
        eye = self.transform_image_top(eye_t)
        eye_s = self.transform_image_side(eye_t_s)
        eye_os = eye_s.transpose(Image.FLIP_LEFT_RIGHT)
        alpha_over(img, eye_s, (5,5), eye_s)
        alpha_over(img, eye_os, (9,5), eye_os)
        alpha_over(img, eye, (0,0), eye)

    return img

# end stone
block(blockid=121, top_image="assets/minecraft/textures/blocks/end_stone.png")

# dragon egg
block(blockid=122, top_image="assets/minecraft/textures/blocks/dragon_egg.png")

# inactive redstone lamp
block(blockid=123, top_image="assets/minecraft/textures/blocks/redstone_lamp_off.png")

# active redstone lamp
block(blockid=124, top_image="assets/minecraft/textures/blocks/redstone_lamp_on.png")

# daylight sensor.
@material(blockid=[151,178], transparent=True)
def daylight_sensor(self, blockid, data):
    if blockid == 151: # daylight sensor
        top = self.load_image_texture("assets/minecraft/textures/blocks/daylight_detector_top.png")
    else: # inverted daylight sensor
        top = self.load_image_texture("assets/minecraft/textures/blocks/daylight_detector_inverted_top.png")
    side = self.load_image_texture("assets/minecraft/textures/blocks/daylight_detector_side.png")

    # cut the side texture in half
    mask = side.crop((0,8,16,16))
    side = Image.new(side.mode, side.size, self.bgcolor)
    alpha_over(side, mask,(0,0,16,8), mask)

    # plain slab
    top = self.transform_image_top(top)
    side = self.transform_image_side(side)
    otherside = side.transpose(Image.FLIP_LEFT_RIGHT)

    sidealpha = side.split()[3]
    side = ImageEnhance.Brightness(side).enhance(0.9)
    side.putalpha(sidealpha)
    othersidealpha = otherside.split()[3]
    otherside = ImageEnhance.Brightness(otherside).enhance(0.8)
    otherside.putalpha(othersidealpha)

    img = Image.new("RGBA", (24,24), self.bgcolor)
    alpha_over(img, side, (0,12), side)
    alpha_over(img, otherside, (12,12), otherside)
    alpha_over(img, top, (0,6), top)

    return img


# wooden double and normal slabs
# these are the new wooden slabs, blockids 43 44 still have wooden
# slabs, but those are unobtainable without cheating
@material(blockid=[125, 126], data=range(16), transparent=(44,), solid=True)
def wooden_slabs(self, blockid, data):
    texture = data & 7
    if texture== 0: # oak
        top = side = self.load_image_texture("assets/minecraft/textures/blocks/planks_oak.png")
    elif texture== 1: # spruce
        top = side = self.load_image_texture("assets/minecraft/textures/blocks/planks_spruce.png")
    elif texture== 2: # birch
        top = side = self.load_image_texture("assets/minecraft/textures/blocks/planks_birch.png")
    elif texture== 3: # jungle
        top = side = self.load_image_texture("assets/minecraft/textures/blocks/planks_jungle.png")
    elif texture== 4: # acacia
        top = side = self.load_image_texture("assets/minecraft/textures/blocks/planks_acacia.png")
    elif texture== 5: # dark wood
        top = side = self.load_image_texture("assets/minecraft/textures/blocks/planks_big_oak.png")
    else:
        return None

    if blockid == 125: # double slab
        return self.build_block(top, side)

    return self.build_slab_block(top, side, data & 8 == 8);

# emerald ore
block(blockid=129, top_image="assets/minecraft/textures/blocks/emerald_ore.png")

# emerald block
block(blockid=133, top_image="assets/minecraft/textures/blocks/emerald_block.png")

# cocoa plant
@material(blockid=127, data=range(12), transparent=True)
def cocoa_plant(self, blockid, data):
    orientation = data & 3
    # rotation
    if self.rotation == 1:
        if orientation == 0: orientation = 1
        elif orientation == 1: orientation = 2
        elif orientation == 2: orientation = 3
        elif orientation == 3: orientation = 0
    elif self.rotation == 2:
        if orientation == 0: orientation = 2
        elif orientation == 1: orientation = 3
        elif orientation == 2: orientation = 0
        elif orientation == 3: orientation = 1
    elif self.rotation == 3:
        if orientation == 0: orientation = 3
        elif orientation == 1: orientation = 0
        elif orientation == 2: orientation = 1
        elif orientation == 3: orientation = 2

    size = data & 12
    if size == 8: # big
        t = self.load_image_texture("assets/minecraft/textures/blocks/cocoa_stage_2.png")
        c_left = (0,3)
        c_right = (8,3)
        c_top = (5,2)
    elif size == 4: # normal
        t = self.load_image_texture("assets/minecraft/textures/blocks/cocoa_stage_1.png")
        c_left = (-2,2)
        c_right = (8,2)
        c_top = (5,2)
    elif size == 0: # small
        t = self.load_image_texture("assets/minecraft/textures/blocks/cocoa_stage_0.png")
        c_left = (-3,2)
        c_right = (6,2)
        c_top = (5,2)

    # let's get every texture piece necessary to do this
    stalk = t.copy()
    ImageDraw.Draw(stalk).rectangle((0,0,11,16),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(stalk).rectangle((12,4,16,16),outline=(0,0,0,0),fill=(0,0,0,0))

    top = t.copy() # warning! changes with plant size
    ImageDraw.Draw(top).rectangle((0,7,16,16),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(top).rectangle((7,0,16,6),outline=(0,0,0,0),fill=(0,0,0,0))

    side = t.copy() # warning! changes with plant size
    ImageDraw.Draw(side).rectangle((0,0,6,16),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(side).rectangle((0,0,16,3),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(side).rectangle((0,14,16,16),outline=(0,0,0,0),fill=(0,0,0,0))

    # first compose the block of the cocoa plant
    block = Image.new("RGBA", (24,24), self.bgcolor)
    tmp = self.transform_image_side(side).transpose(Image.FLIP_LEFT_RIGHT)
    alpha_over (block, tmp, c_right,tmp) # right side
    tmp = tmp.transpose(Image.FLIP_LEFT_RIGHT)
    alpha_over (block, tmp, c_left,tmp) # left side
    tmp = self.transform_image_top(top)
    alpha_over(block, tmp, c_top,tmp)
    if size == 0:
        # fix a pixel hole
        block.putpixel((6,9), block.getpixel((6,10)))

    # compose the cocoa plant
    img = Image.new("RGBA", (24,24), self.bgcolor)
    if orientation in (2,3): # south and west
        tmp = self.transform_image_side(stalk).transpose(Image.FLIP_LEFT_RIGHT)
        alpha_over(img, block,(-1,-2), block)
        alpha_over(img, tmp, (4,-2), tmp)
        if orientation == 3:
            img = img.transpose(Image.FLIP_LEFT_RIGHT)
    elif orientation in (0,1): # north and east
        tmp = self.transform_image_side(stalk.transpose(Image.FLIP_LEFT_RIGHT))
        alpha_over(img, block,(-1,5), block)
        alpha_over(img, tmp, (2,12), tmp)
        if orientation == 0:
            img = img.transpose(Image.FLIP_LEFT_RIGHT)

    return img

# command block
@material(blockid=[137,210,211], solid=True, nodata=True)
def command_block(self, blockid, data):
    if blockid == 210:
        front = self.load_image_texture("assets/minecraft/textures/blocks/repeating_command_block_front.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/repeating_command_block_side.png")
        back = self.load_image_texture("assets/minecraft/textures/blocks/repeating_command_block_back.png")
    elif blockid == 211:
        front = self.load_image_texture("assets/minecraft/textures/blocks/chain_command_block_front.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/chain_command_block_side.png")
        back = self.load_image_texture("assets/minecraft/textures/blocks/chain_command_block_back.png")
    else:
        front = self.load_image_texture("assets/minecraft/textures/blocks/command_block_front.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/command_block_side.png")
        back = self.load_image_texture("assets/minecraft/textures/blocks/command_block_back.png")
    return self.build_full_block(side, side, back, front, side)

# beacon block
# at the moment of writing this, it seems the beacon block doens't use
# the data values
@material(blockid=138, transparent=True, nodata = True)
def beacon(self, blockid, data):
    # generate the three pieces of the block
    t = self.load_image_texture("assets/minecraft/textures/blocks/glass.png")
    glass = self.build_block(t,t)
    t = self.load_image_texture("assets/minecraft/textures/blocks/obsidian.png")
    obsidian = self.build_full_block((t,12),None, None, t, t)
    obsidian = obsidian.resize((20,20), Image.ANTIALIAS)
    t = self.load_image_texture("assets/minecraft/textures/blocks/beacon.png")
    crystal = self.build_block(t,t)
    crystal = crystal.resize((16,16),Image.ANTIALIAS)

    # compose the block
    img = Image.new("RGBA", (24,24), self.bgcolor)
    alpha_over(img, obsidian, (2, 4), obsidian)
    alpha_over(img, crystal, (4,3), crystal)
    alpha_over(img, glass, (0,0), glass)

    return img

# cobblestone and mossy cobblestone walls, chorus plants
# one additional bit of data value added for mossy and cobblestone
@material(blockid=[139, 199], data=range(32), transparent=True, nospawn=True)
def cobblestone_wall(self, blockid, data):
    # chorus plants
    if blockid == 199:
        t = self.load_image_texture("assets/minecraft/textures/blocks/chorus_plant.png").copy()
    # no rotation, uses pseudo data
    elif data & 0b10000 == 0:
        # cobblestone
        t = self.load_image_texture("assets/minecraft/textures/blocks/cobblestone.png").copy()
    else:
        # mossy cobblestone
        t = self.load_image_texture("assets/minecraft/textures/blocks/cobblestone_mossy.png").copy()

    wall_pole_top = t.copy()
    wall_pole_side = t.copy()
    wall_side_top = t.copy()
    wall_side = t.copy()
    # _full is used for walls without pole
    wall_side_top_full = t.copy()
    wall_side_full = t.copy()

    # generate the textures of the wall
    ImageDraw.Draw(wall_pole_top).rectangle((0,0,3,15),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(wall_pole_top).rectangle((12,0,15,15),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(wall_pole_top).rectangle((0,0,15,3),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(wall_pole_top).rectangle((0,12,15,15),outline=(0,0,0,0),fill=(0,0,0,0))

    ImageDraw.Draw(wall_pole_side).rectangle((0,0,3,15),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(wall_pole_side).rectangle((12,0,15,15),outline=(0,0,0,0),fill=(0,0,0,0))

    # Create the sides and the top of the pole
    wall_pole_side = self.transform_image_side(wall_pole_side)
    wall_pole_other_side = wall_pole_side.transpose(Image.FLIP_LEFT_RIGHT)
    wall_pole_top = self.transform_image_top(wall_pole_top)

    # Darken the sides slightly. These methods also affect the alpha layer,
    # so save them first (we don't want to "darken" the alpha layer making
    # the block transparent)
    sidealpha = wall_pole_side.split()[3]
    wall_pole_side = ImageEnhance.Brightness(wall_pole_side).enhance(0.8)
    wall_pole_side.putalpha(sidealpha)
    othersidealpha = wall_pole_other_side.split()[3]
    wall_pole_other_side = ImageEnhance.Brightness(wall_pole_other_side).enhance(0.7)
    wall_pole_other_side.putalpha(othersidealpha)

    # Compose the wall pole
    wall_pole = Image.new("RGBA", (24,24), self.bgcolor)
    alpha_over(wall_pole,wall_pole_side, (3,4),wall_pole_side)
    alpha_over(wall_pole,wall_pole_other_side, (9,4),wall_pole_other_side)
    alpha_over(wall_pole,wall_pole_top, (0,0),wall_pole_top)

    # create the sides and the top of a wall attached to a pole
    ImageDraw.Draw(wall_side).rectangle((0,0,15,2),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(wall_side).rectangle((0,0,11,15),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(wall_side_top).rectangle((0,0,11,15),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(wall_side_top).rectangle((0,0,15,4),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(wall_side_top).rectangle((0,11,15,15),outline=(0,0,0,0),fill=(0,0,0,0))
    # full version, without pole
    ImageDraw.Draw(wall_side_full).rectangle((0,0,15,2),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(wall_side_top_full).rectangle((0,4,15,15),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(wall_side_top_full).rectangle((0,4,15,15),outline=(0,0,0,0),fill=(0,0,0,0))

    # compose the sides of a wall atached to a pole
    tmp = Image.new("RGBA", (24,24), self.bgcolor)
    wall_side = self.transform_image_side(wall_side)
    wall_side_top = self.transform_image_top(wall_side_top)

    # Darken the sides slightly. These methods also affect the alpha layer,
    # so save them first (we don't want to "darken" the alpha layer making
    # the block transparent)
    sidealpha = wall_side.split()[3]
    wall_side = ImageEnhance.Brightness(wall_side).enhance(0.7)
    wall_side.putalpha(sidealpha)

    alpha_over(tmp,wall_side, (0,0),wall_side)
    alpha_over(tmp,wall_side_top, (-5,3),wall_side_top)
    wall_side = tmp
    wall_other_side = wall_side.transpose(Image.FLIP_LEFT_RIGHT)

    # compose the sides of the full wall
    tmp = Image.new("RGBA", (24,24), self.bgcolor)
    wall_side_full = self.transform_image_side(wall_side_full)
    wall_side_top_full = self.transform_image_top(wall_side_top_full.rotate(90))

    # Darken the sides slightly. These methods also affect the alpha layer,
    # so save them first (we don't want to "darken" the alpha layer making
    # the block transparent)
    sidealpha = wall_side_full.split()[3]
    wall_side_full = ImageEnhance.Brightness(wall_side_full).enhance(0.7)
    wall_side_full.putalpha(sidealpha)

    alpha_over(tmp,wall_side_full, (4,0),wall_side_full)
    alpha_over(tmp,wall_side_top_full, (3,-4),wall_side_top_full)
    wall_side_full = tmp
    wall_other_side_full = wall_side_full.transpose(Image.FLIP_LEFT_RIGHT)

    # Create img to compose the wall
    img = Image.new("RGBA", (24,24), self.bgcolor)

    # Position wall imgs around the wall bit stick
    pos_top_left = (-5,-2)
    pos_bottom_left = (-8,4)
    pos_top_right = (5,-3)
    pos_bottom_right = (7,4)

    # +x axis points top right direction
    # +y axis points bottom right direction
    # There are two special cases for wall without pole.
    # Normal case:
    # First compose the walls in the back of the image,
    # then the pole and then the walls in the front.
    if (data == 0b1010) or (data == 0b11010):
        alpha_over(img, wall_other_side_full,(0,2), wall_other_side_full)
    elif (data == 0b0101) or (data == 0b10101):
        alpha_over(img, wall_side_full,(0,2), wall_side_full)
    else:
        if (data & 0b0001) == 1:
            alpha_over(img,wall_side, pos_top_left,wall_side)                # top left
        if (data & 0b1000) == 8:
            alpha_over(img,wall_other_side, pos_top_right,wall_other_side)    # top right

        alpha_over(img,wall_pole,(0,0),wall_pole)

        if (data & 0b0010) == 2:
            alpha_over(img,wall_other_side, pos_bottom_left,wall_other_side)      # bottom left
        if (data & 0b0100) == 4:
            alpha_over(img,wall_side, pos_bottom_right,wall_side)                  # bottom right

    return img

# carrots, potatoes
@material(blockid=[141,142], data=range(8), transparent=True, nospawn=True)
def crops4(self, blockid, data):
    # carrots and potatoes have 8 data, but only 4 visual stages
    stage = {0:0,
             1:0,
             2:1,
             3:1,
             4:2,
             5:2,
             6:2,
             7:3}[data]
    if blockid == 141: # carrots
        raw_crop = self.load_image_texture("assets/minecraft/textures/blocks/carrots_stage_%d.png" % stage)
    else: # potatoes
        raw_crop = self.load_image_texture("assets/minecraft/textures/blocks/potatoes_stage_%d.png" % stage)
    crop1 = self.transform_image_top(raw_crop)
    crop2 = self.transform_image_side(raw_crop)
    crop3 = crop2.transpose(Image.FLIP_LEFT_RIGHT)

    img = Image.new("RGBA", (24,24), self.bgcolor)
    alpha_over(img, crop1, (0,12), crop1)
    alpha_over(img, crop2, (6,3), crop2)
    alpha_over(img, crop3, (6,3), crop3)
    return img

# anvils
@material(blockid=145, data=range(12), transparent=True)
def anvil(self, blockid, data):

    # anvils only have two orientations, invert it for rotations 1 and 3
    orientation = data & 0x1
    if self.rotation in (1,3):
        if orientation == 1:
            orientation = 0
        else:
            orientation = 1

    # get the correct textures
    # the bits 0x4 and 0x8 determine how damaged is the anvil
    if (data & 0xc) == 0: # non damaged anvil
        top = self.load_image_texture("assets/minecraft/textures/blocks/anvil_top_damaged_0.png")
    elif (data & 0xc) == 0x4: # slightly damaged
        top = self.load_image_texture("assets/minecraft/textures/blocks/anvil_top_damaged_1.png")
    elif (data & 0xc) == 0x8: # very damaged
        top = self.load_image_texture("assets/minecraft/textures/blocks/anvil_top_damaged_2.png")
    # everything else use this texture
    big_side = self.load_image_texture("assets/minecraft/textures/blocks/anvil_base.png").copy()
    small_side = self.load_image_texture("assets/minecraft/textures/blocks/anvil_base.png").copy()
    base = self.load_image_texture("assets/minecraft/textures/blocks/anvil_base.png").copy()
    small_base = self.load_image_texture("assets/minecraft/textures/blocks/anvil_base.png").copy()

    # cut needed patterns
    ImageDraw.Draw(big_side).rectangle((0,8,15,15),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(small_side).rectangle((0,0,2,15),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(small_side).rectangle((13,0,15,15),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(small_side).rectangle((0,8,15,15),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(base).rectangle((0,0,15,15),outline=(0,0,0,0))
    ImageDraw.Draw(base).rectangle((1,1,14,14),outline=(0,0,0,0))
    ImageDraw.Draw(small_base).rectangle((0,0,15,15),outline=(0,0,0,0))
    ImageDraw.Draw(small_base).rectangle((1,1,14,14),outline=(0,0,0,0))
    ImageDraw.Draw(small_base).rectangle((2,2,13,13),outline=(0,0,0,0))
    ImageDraw.Draw(small_base).rectangle((3,3,12,12),outline=(0,0,0,0))

    # check orientation and compose the anvil
    if orientation == 1: # bottom-left top-right
        top = top.rotate(90)
        left_side = small_side
        left_pos = (1,7)
        right_side = big_side
        right_pos = (10,5)
    else: # top-left bottom-right
        right_side = small_side
        right_pos = (12,7)
        left_side = big_side
        left_pos = (3,5)

    img = Image.new("RGBA", (24,24), self.bgcolor)

    # darken sides
    alpha = big_side.split()[3]
    big_side = ImageEnhance.Brightness(big_side).enhance(0.8)
    big_side.putalpha(alpha)
    alpha = small_side.split()[3]
    small_side = ImageEnhance.Brightness(small_side).enhance(0.9)
    small_side.putalpha(alpha)
    alpha = base.split()[3]
    base_d = ImageEnhance.Brightness(base).enhance(0.8)
    base_d.putalpha(alpha)

    # compose
    base = self.transform_image_top(base)
    base_d = self.transform_image_top(base_d)
    small_base = self.transform_image_top(small_base)
    top = self.transform_image_top(top)

    alpha_over(img, base_d, (0,12), base_d)
    alpha_over(img, base_d, (0,11), base_d)
    alpha_over(img, base_d, (0,10), base_d)
    alpha_over(img, small_base, (0,10), small_base)

    alpha_over(img, top, (0,0), top)

    left_side = self.transform_image_side(left_side)
    right_side = self.transform_image_side(right_side).transpose(Image.FLIP_LEFT_RIGHT)

    alpha_over(img, left_side, left_pos, left_side)
    alpha_over(img, right_side, right_pos, right_side)

    return img


# block of redstone
block(blockid=152, top_image="assets/minecraft/textures/blocks/redstone_block.png")

# nether quartz ore
block(blockid=153, top_image="assets/minecraft/textures/blocks/quartz_ore.png")

# block of quartz
@material(blockid=155, data=range(5), solid=True)
def quartz_block(self, blockid, data):

    if data in (0,1): # normal and chiseled quartz block
        if data == 0:
            top = self.load_image_texture("assets/minecraft/textures/blocks/quartz_block_top.png")
            side = self.load_image_texture("assets/minecraft/textures/blocks/quartz_block_side.png")
        else:
            top = self.load_image_texture("assets/minecraft/textures/blocks/quartz_block_chiseled_top.png")
            side = self.load_image_texture("assets/minecraft/textures/blocks/quartz_block_chiseled.png")
        return self.build_block(top, side)

    # pillar quartz block with orientation
    top = self.load_image_texture("assets/minecraft/textures/blocks/quartz_block_lines_top.png")
    side = self.load_image_texture("assets/minecraft/textures/blocks/quartz_block_lines.png").copy()
    if data == 2: # vertical
        return self.build_block(top, side)
    elif data == 3: # north-south oriented
        if self.rotation in (0,2):
            return self.build_full_block(side, None, None, top, side.rotate(90))
        return self.build_full_block(side.rotate(90), None, None, side.rotate(90), top)

    elif data == 4: # east-west oriented
        if self.rotation in (0,2):
            return self.build_full_block(side.rotate(90), None, None, side.rotate(90), top)
        return self.build_full_block(side, None, None, top, side.rotate(90))

# hopper
@material(blockid=154, data=range(4), transparent=True)
def hopper(self, blockid, data):
    #build the top
    side = self.load_image_texture("assets/minecraft/textures/blocks/hopper_outside.png")
    top = self.load_image_texture("assets/minecraft/textures/blocks/hopper_top.png")
    bottom = self.load_image_texture("assets/minecraft/textures/blocks/hopper_inside.png")
    hop_top = self.build_full_block((top,10), side, side, side, side, side)

    #build a solid block for mid/top
    hop_mid = self.build_full_block((top,5), side, side, side, side, side)
    hop_bot = self.build_block(side,side)

    hop_mid = hop_mid.resize((17,17),Image.ANTIALIAS)
    hop_bot = hop_bot.resize((10,10),Image.ANTIALIAS)

    #compose the final block
    img = Image.new("RGBA", (24,24), self.bgcolor)
    alpha_over(img, hop_bot, (7,14), hop_bot)
    alpha_over(img, hop_mid, (3,3), hop_mid)
    alpha_over(img, hop_top, (0,-6), hop_top)

    return img

# slime block
block(blockid=165, top_image="assets/minecraft/textures/blocks/slime.png")

# prismarine block
@material(blockid=168, data=range(3), solid=True)
def prismarine_block(self, blockid, data):

   if data == 0: # prismarine
       t = self.load_image_texture("assets/minecraft/textures/blocks/prismarine_rough.png")
   elif data == 1: # prismarine bricks
       t = self.load_image_texture("assets/minecraft/textures/blocks/prismarine_bricks.png")
   elif data == 2: # dark prismarine
       t = self.load_image_texture("assets/minecraft/textures/blocks/prismarine_dark.png")

   img = self.build_block(t, t)

   return img

# sea lantern
block(blockid=169, top_image="assets/minecraft/textures/blocks/sea_lantern.png")

# hay block
@material(blockid=170, data=range(9), solid=True)
def hayblock(self, blockid, data):
    top = self.load_image_texture("assets/minecraft/textures/blocks/hay_block_top.png")
    side = self.load_image_texture("assets/minecraft/textures/blocks/hay_block_side.png")

    if self.rotation == 1:
        if data == 4: data = 8
        elif data == 8: data = 4
    elif self.rotation == 3:
        if data == 4: data = 8
        elif data == 8: data = 4

    # choose orientation and paste textures
    if data == 4: # east-west orientation
        return self.build_full_block(side.rotate(90), None, None, top, side.rotate(90))
    elif data == 8: # north-south orientation
        return self.build_full_block(side, None, None, side.rotate(90), top)
    else:
        return self.build_block(top, side)


# carpet - wool block that's small?
@material(blockid=171, data=range(16), transparent=True)
def carpet(self, blockid, data):
    texture = self.load_image_texture("assets/minecraft/textures/blocks/wool_colored_%s.png" % color_map[data])

    return self.build_full_block((texture,15),texture,texture,texture,texture)

#clay block
block(blockid=172, top_image="assets/minecraft/textures/blocks/hardened_clay.png")

#stained hardened clay
@material(blockid=159, data=range(16), solid=True)
def stained_clay(self, blockid, data):
    texture = self.load_image_texture("assets/minecraft/textures/blocks/hardened_clay_stained_%s.png" % color_map[data])

    return self.build_block(texture,texture)

#coal block
block(blockid=173, top_image="assets/minecraft/textures/blocks/coal_block.png")

# packed ice block
block(blockid=174, top_image="assets/minecraft/textures/blocks/ice_packed.png")

@material(blockid=175, data=range(16), transparent=True)
def flower(self, blockid, data):
    double_plant_map = ["sunflower", "syringa", "grass", "fern", "rose", "paeonia", "paeonia", "paeonia"]
    plant = double_plant_map[data & 0x7]

    if data & 0x8:
        part = "top"
    else:
        part = "bottom"

    png = "assets/minecraft/textures/blocks/double_plant_%s_%s.png" % (plant,part)
    texture = self.load_image_texture(png)
    img = self.build_billboard(texture)

    #sunflower top
    if data == 8:
        bloom_tex = self.load_image_texture("assets/minecraft/textures/blocks/double_plant_sunflower_front.png")
        alpha_over(img, bloom_tex.resize((14, 11), Image.ANTIALIAS), (5,5))

    return img

# chorus flower
@material(blockid=200, data=range(6), solid=True)
def chorus_flower(self, blockid, data):
    # aged 5, dead
    if data == 5:
        texture = self.load_image_texture("assets/minecraft/textures/blocks/chorus_flower_dead.png")
    else:
        texture = self.load_image_texture("assets/minecraft/textures/blocks/chorus_flower.png")

    return self.build_block(texture,texture)

# purpur block
block(blockid=201, top_image="assets/minecraft/textures/blocks/purpur_block.png")

# purpur pilar
@material(blockid=202, data=range(12) , solid=True)
def purpur_pillar(self, blockid, data):
    pillar_orientation = data & 12
    top=self.load_image_texture("assets/minecraft/textures/blocks/purpur_pillar_top.png")
    side=self.load_image_texture("assets/minecraft/textures/blocks/purpur_pillar.png")
    if pillar_orientation == 0: # east-west orientation
        return self.build_block(top, side)
    elif pillar_orientation == 4: # east-west orientation
        return self.build_full_block(side.rotate(90), None, None, top, side.rotate(90))
    elif pillar_orientation == 8: # north-south orientation

        return self.build_full_block(side, None, None, side.rotate(270), top)

# end brick
block(blockid=206, top_image="assets/minecraft/textures/blocks/end_bricks.png")

# frosted ice
@material(blockid=212, data=range(4), solid=True)
def frosted_ice(self, blockid, data):
    img = self.load_image_texture("assets/minecraft/textures/blocks/frosted_ice_%d.png" % data)
    return self.build_block(img, img)

# magma block
block(blockid=213, top_image="assets/minecraft/textures/blocks/magma.png")

# nether wart block
block(blockid=214, top_image="assets/minecraft/textures/blocks/nether_wart_block.png")

# red nether brick
block(blockid=215, top_image="assets/minecraft/textures/blocks/red_nether_brick.png")

@material(blockid=216, data=range(12), solid=True)
def boneblock(self, blockid, data):
    # extract orientation
    boneblock_orientation = data & 12
    if self.rotation == 1:
        if boneblock_orientation == 4: boneblock_orientation = 8
        elif boneblock_orientation == 8: boneblock_orientation = 4
    elif self.rotation == 3:
        if boneblock_orientation == 4: boneblock_orientation = 8
        elif boneblock_orientation == 8: boneblock_orientation = 4

    top = self.load_image_texture("assets/minecraft/textures/blocks/bone_block_top.png")
    side = self.load_image_texture("assets/minecraft/textures/blocks/bone_block_side.png")

    # choose orientation and paste textures
    if boneblock_orientation == 0:
        return self.build_block(top, side)
    elif boneblock_orientation == 4: # east-west orientation
        return self.build_full_block(side.rotate(90), None, None, top, side.rotate(90))
    elif boneblock_orientation == 8: # north-south orientation
        return self.build_full_block(side, None, None, side.rotate(270), top)

# observer
@material(blockid=218, data=range(6), solid=True, nospawn=True)
def observer(self, blockid, data):
    # first, do the rotation if needed
    if self.rotation == 1:
        if data == 2: data = 5
        elif data == 3: data = 4
        elif data == 4: data = 2
        elif data == 5: data = 3
    elif self.rotation == 2:
        if data == 2: data = 3
        elif data == 3: data = 2
        elif data == 4: data = 5
        elif data == 5: data = 4
    elif self.rotation == 3:
        if data == 2: data = 4
        elif data == 3: data = 5
        elif data == 4: data = 3
        elif data == 5: data = 2

    front = self.load_image_texture("assets/minecraft/textures/blocks/observer_front.png").copy()
    side = self.load_image_texture("assets/minecraft/textures/blocks/observer_side.png").copy()
    back = self.load_image_texture("assets/minecraft/textures/blocks/observer_back.png").copy()
    top = self.load_image_texture("assets/minecraft/textures/blocks/observer_top.png").copy()

    if data == 0: # down
        side = side.rotate(90)
        img = self.build_full_block(back, None, None, side, top)
    elif data == 1: # up
        side = side.rotate(90)
        img = self.build_full_block(front.rotate(180), None, None, side, top.rotate(180))
    elif data == 2: # east
        img = self.build_full_block(top.rotate(180), None, None, side, back)
    elif data == 3: # west
        img = self.build_full_block(top, None, None, side, front)
    elif data == 4: # north
        img = self.build_full_block(top.rotate(270), None, None, front, side)
    elif data == 5: # south
        img = self.build_full_block(top.rotate(90), None, None, back, side)

    return img

# shulker box
@material(blockid=range(219,235), data=range(6), solid=True, nospawn=True)
def shulker_box(self, blockid, data):
    # first, do the rotation if needed
    data = data & 7
    if self.rotation == 1:
        if data == 2: data = 5
        elif data == 3: data = 4
        elif data == 4: data = 2
        elif data == 5: data = 3
    elif self.rotation == 2:
        if data == 2: data = 3
        elif data == 3: data = 2
        elif data == 4: data = 5
        elif data == 5: data = 4
    elif self.rotation == 3:
        if data == 2: data = 4
        elif data == 3: data = 5
        elif data == 4: data = 3
        elif data == 5: data = 2

    color = color_map[blockid - 219]
    shulker_t = self.load_image_texture("assets/minecraft/textures/entity/shulker/shulker_%s.png" % color).copy()
    w,h = shulker_t.size
    res = w / 4
    # Cut out the parts of the shulker texture we need for the box
    top = shulker_t.crop((res,0,res*2,res))
    bottom = shulker_t.crop((res*2,int(res*1.75),res*3,int(res*2.75)))
    side_top = shulker_t.crop((0,res,res,int(res*1.75)))
    side_bottom = shulker_t.crop((0,int(res*2.75),res,int(res*3.25)))
    side = Image.new('RGBA', (res,res))
    side.paste(side_top, (0,0), side_top)
    side.paste(side_bottom, (0,res/2), side_bottom)

    if data == 0: # down
        side = side.rotate(180)
        img = self.build_full_block(bottom, None, None, side, side)
    elif data == 1: # up
        img = self.build_full_block(top, None, None, side, side)
    elif data == 2: # east
        img = self.build_full_block(side, None, None, side.rotate(90), bottom)
    elif data == 3: # west
        img = self.build_full_block(side.rotate(180), None, None, side.rotate(270), top)
    elif data == 4: # north
        img = self.build_full_block(side.rotate(90), None, None, top, side.rotate(270))
    elif data == 5: # south
        img = self.build_full_block(side.rotate(270), None, None, bottom, side.rotate(90))

    return img

# structure block
@material(blockid=255, data=range(4), solid=True)
def structure_block(self, blockid, data):
    if data == 0:
        img = self.load_image_texture("assets/minecraft/textures/blocks/structure_block_save.png")
    elif data == 1:
        img = self.load_image_texture("assets/minecraft/textures/blocks/structure_block_load.png")
    elif data == 2:
        img = self.load_image_texture("assets/minecraft/textures/blocks/structure_block_corner.png")
    elif data == 3:
        img = self.load_image_texture("assets/minecraft/textures/blocks/structure_block_data.png")
    return self.build_block(img, img)

# beetroots
@material(blockid=207, data=range(4), transparent=True, nospawn=True)
def crops(self, blockid, data):
    raw_crop = self.load_image_texture("assets/minecraft/textures/blocks/beetroots_stage_%d.png" % data)
    crop1 = self.transform_image_top(raw_crop)
    crop2 = self.transform_image_side(raw_crop)
    crop3 = crop2.transpose(Image.FLIP_LEFT_RIGHT)

    img = Image.new("RGBA", (24,24), self.bgcolor)
    alpha_over(img, crop1, (0,12), crop1)
    alpha_over(img, crop2, (6,3), crop2)
    alpha_over(img, crop3, (6,3), crop3)
    return img

# Concrete
@material(blockid=251, data=range(16), solid=True)
def concrete(self, blockid, data):
    texture = self.load_image_texture("assets/minecraft/textures/blocks/concrete_%s.png" % color_map[data])
    return self.build_block(texture, texture)

# Concrete Powder
@material(blockid=252, data=range(16), solid=True)
def concrete(self, blockid, data):
    texture = self.load_image_texture("assets/minecraft/textures/blocks/concrete_powder_%s.png" % color_map[data])
    return self.build_block(texture, texture)

# Glazed Terracotta
@material(blockid=range(235,251), data=range(8), solid=True)
def glazed_terracotta(self, blockid, data):
    texture = self.load_image_texture("assets/minecraft/textures/blocks/glazed_terracotta_%s.png" % color_map[blockid - 235])
    glazed_terracotta_orientation = data & 3
    
    # Glazed Terracotta rotations are need seperate handling for each render direction

    if self.rotation == 0: # rendering north upper-left
        # choose orientation and paste textures
        if glazed_terracotta_orientation == 0: # south / Player was facing North
            return self.build_block(texture, texture)
        elif glazed_terracotta_orientation == 1: # west / Player was facing east
            return self.build_full_block(texture.rotate(270), None, None, texture.rotate(90), texture.rotate(270))
        elif glazed_terracotta_orientation == 2: # North / Player was facing South
            return self.build_full_block(texture.rotate(180), None, None, texture.rotate(180), texture.rotate(180))
        elif glazed_terracotta_orientation == 3: # east / Player was facing west
            return self.build_full_block(texture.rotate(90), None, None, texture.rotate(270), texture.rotate(90))

    elif self.rotation == 1: # north upper-right
        # choose orientation and paste textures
        if glazed_terracotta_orientation == 0: # south / Player was facing North
            return self.build_full_block(texture.rotate(270), None, None, texture.rotate(90), texture.rotate(270))
        elif glazed_terracotta_orientation == 1: # west / Player was facing east
            return self.build_full_block(texture.rotate(180), None, None, texture.rotate(180), texture.rotate(180))
        elif glazed_terracotta_orientation == 2: # North / Player was facing South
            return self.build_full_block(texture.rotate(90), None, None, texture.rotate(270), texture.rotate(90))
        elif glazed_terracotta_orientation == 3: # east / Player was facing west
            return self.build_block(texture, texture)
            

    elif self.rotation == 2: # north lower-right
        # choose orientation and paste textures
        if glazed_terracotta_orientation == 0: # south / Player was facing North
            return self.build_full_block(texture.rotate(180), None, None, texture.rotate(180), texture.rotate(180))
        elif glazed_terracotta_orientation == 1: # west / Player was facing east
            return self.build_full_block(texture.rotate(90), None, None, texture.rotate(270), texture.rotate(90))
        elif glazed_terracotta_orientation == 2: # North / Player was facing South
            return self.build_block(texture, texture)
        elif glazed_terracotta_orientation == 3: # east / Player was facing west
            return self.build_full_block(texture.rotate(270), None, None, texture.rotate(90), texture.rotate(270))
            
    elif self.rotation == 3: # north lower-left
        # choose orientation and paste textures
        if glazed_terracotta_orientation == 0: # south / Player was facing North
            return self.build_full_block(texture.rotate(90), None, None, texture.rotate(270), texture.rotate(90))
        elif glazed_terracotta_orientation == 1: # west / Player was facing east
            return self.build_block(texture, texture)
        elif glazed_terracotta_orientation == 2: # North / Player was facing South
            return self.build_full_block(texture.rotate(270), None, None, texture.rotate(90), texture.rotate(270))
        elif glazed_terracotta_orientation == 3: # east / Player was facing west
            return self.build_full_block(texture.rotate(180), None, None, texture.rotate(180), texture.rotate(180))
################################################################################
#                                   Minefield                                  #
################################################################################

# escaliers colores
@material(blockid=[1000,1001,1002,1003,1004,1005,1006,1007], data=range(256), transparent=True, solid=True, nospawn=True)
def stairs(self, blockid, data):
    color = ((blockid - 1000) * 2) + (data & 0x1)

    data = data >> 1;
    # preserve the upside-down bit
    upside_down = data & 0x4

    # find solid quarters within the top or bottom half of the block
    #                   NW           NE           SE           SW
    quarters = [data & 0x8, data & 0x10, data & 0x20, data & 0x40]

    # rotate the quarters so we can pretend northdirection is always upper-left
    numpy.roll(quarters, [0,1,3,2][self.rotation])
    nw,ne,se,sw = quarters

    if color == 0: # white
        texture = texture = self.load_image_texture("assets/minecraft/textures/blocks/cloth_0.png").copy()
    elif color == 1: # orange
        texture = texture = self.load_image_texture("assets/minecraft/textures/blocks/cloth_1.png").copy()
    elif color == 2: # magenta
        texture = texture = self.load_image_texture("assets/minecraft/textures/blocks/cloth_2.png").copy()
    elif color == 3: # light blue
        texture = texture = self.load_image_texture("assets/minecraft/textures/blocks/cloth_3.png").copy()
    elif color == 4: # yellow
        texture = texture = self.load_image_texture("assets/minecraft/textures/blocks/cloth_4.png").copy()
    elif color == 5: # light green
        texture = texture = self.load_image_texture("assets/minecraft/textures/blocks/cloth_5.png").copy()
    elif color == 6: # pink
        texture = texture = self.load_image_texture("assets/minecraft/textures/blocks/cloth_6.png").copy()
    elif color == 7: # grey
        texture = texture = self.load_image_texture("assets/minecraft/textures/blocks/cloth_7.png").copy()
    elif color == 8: # light grey
        texture = texture = self.load_image_texture("assets/minecraft/textures/blocks/cloth_8.png").copy()
    elif color == 9: # cyan
        texture = texture = self.load_image_texture("assets/minecraft/textures/blocks/cloth_9.png").copy()
    elif color == 10: # purple
        texture = texture = self.load_image_texture("assets/minecraft/textures/blocks/cloth_10.png").copy()
    elif color == 11: # blue
        texture = texture = self.load_image_texture("assets/minecraft/textures/blocks/cloth_11.png").copy()
    elif color == 12: # brown
        texture = texture = self.load_image_texture("assets/minecraft/textures/blocks/cloth_12.png").copy()
    elif color == 13: # dark green
        texture = texture = self.load_image_texture("assets/minecraft/textures/blocks/cloth_13.png").copy()
    elif color == 14: # red
        texture = texture = self.load_image_texture("assets/minecraft/textures/blocks/cloth_14.png").copy()
    elif color == 15: # black
        texture = texture = self.load_image_texture("assets/minecraft/textures/blocks/cloth_15.png").copy()

    outside_l = texture.copy()
    outside_r = texture.copy()
    inside_l = texture.copy()
    inside_r = texture.copy()

    # sandstone & quartz stairs have special top texture
    if blockid == 128:
        texture = self.load_image_texture("assets/minecraft/textures/blocks/sandstone_top.png").copy()
    elif blockid == 156:
        texture = self.load_image_texture("assets/minecraft/textures/blocks/quartz_block_top.png").copy()

    slab_top = texture.copy()

    push = 8 if upside_down else 0

    def rect(tex,coords):
        ImageDraw.Draw(tex).rectangle(coords,outline=(0,0,0,0),fill=(0,0,0,0))

    # cut out top or bottom half from inner surfaces
    rect(inside_l, (0,8-push,15,15-push))
    rect(inside_r, (0,8-push,15,15-push))

    # cut out missing or obstructed quarters from each surface
    if not nw:
        rect(outside_l, (0,push,7,7+push))
        rect(texture, (0,0,7,7))
    if not nw or sw:
        rect(inside_r, (8,push,15,7+push)) # will be flipped
    if not ne:
        rect(texture, (8,0,15,7))
    if not ne or nw:
        rect(inside_l, (0,push,7,7+push))
    if not ne or se:
        rect(inside_r, (0,push,7,7+push)) # will be flipped
    if not se:
        rect(outside_r, (0,push,7,7+push)) # will be flipped
        rect(texture, (8,8,15,15))
    if not se or sw:
        rect(inside_l, (8,push,15,7+push))
    if not sw:
        rect(outside_l, (8,push,15,7+push))
        rect(outside_r, (8,push,15,7+push)) # will be flipped
        rect(texture, (0,8,7,15))

    img = Image.new("RGBA", (24,24), self.bgcolor)

    if upside_down:
        # top should have no cut-outs after all
        texture = slab_top
    else:
        # render the slab-level surface
        slab_top = self.transform_image_top(slab_top)
        alpha_over(img, slab_top, (0,6))

    # render inner left surface
    inside_l = self.transform_image_side(inside_l)
    # Darken the vertical part of the second step
    sidealpha = inside_l.split()[3]
    # darken it a bit more than usual, looks better
    inside_l = ImageEnhance.Brightness(inside_l).enhance(0.8)
    inside_l.putalpha(sidealpha)
    alpha_over(img, inside_l, (6,3))

    # render inner right surface
    inside_r = self.transform_image_side(inside_r).transpose(Image.FLIP_LEFT_RIGHT)
    # Darken the vertical part of the second step
    sidealpha = inside_r.split()[3]
    # darken it a bit more than usual, looks better
    inside_r = ImageEnhance.Brightness(inside_r).enhance(0.7)
    inside_r.putalpha(sidealpha)
    alpha_over(img, inside_r, (6,3))

    # render outer surfaces
    alpha_over(img, self.build_full_block(texture, None, None, outside_l, outside_r))

    return img

# paille
block(blockid=1010, top_image="assets/minecraft/textures/blocks/paille.png")

# ardoise
block(blockid=1013, top_image="assets/minecraft/textures/blocks/ardoise.png")

# glowstone pure
block(blockid=1017, top_image="assets/minecraft/textures/blocks/glowstone-pure.png")

# glowstone pure lanterne
block(blockid=1019, top_image="assets/minecraft/textures/blocks/lanterne-glowstone-pure.png")

# glowstone lanterne
block(blockid=1018, top_image="assets/minecraft/textures/blocks/lanterne-glowstone.png")

@material(blockid=[1016,1121], data=range(16), transparent=True, solid=True)
def slabs(self, blockid, data):
    textureData = data & 0x7;
    if textureData == 0: # spruce
        top = side = self.load_image_texture("assets/minecraft/textures/blocks/planks_spruce.png")
    elif textureData == 1: # birch
        top = side = self.load_image_texture("assets/minecraft/textures/blocks/planks_birch.png")
    elif textureData == 2: # jungle
        top = side = self.load_image_texture("assets/minecraft/textures/blocks/planks_jungle.png")
    elif textureData == 3: # paille
        top = side = self.load_image_texture("assets/minecraft/textures/blocks/paille.png")
    elif textureData == 4: # ardoise
        top = side = self.load_image_texture("assets/minecraft/textures/blocks/ardoise.png")
    elif textureData == 5: # nether
        top = side = self.load_image_texture("assets/minecraft/textures/blocks/netherBrick.png")
    elif textureData == 6: # marbre blanc
        top = side = self.load_image_texture("assets/minecraft/textures/blocks/marbre-white-halfstep-top.png")
    elif textureData == 7: # marbre
        top = side = self.load_image_texture("assets/minecraft/textures/blocks/marbre-noir-halfstep-top.png")
    else:
        return None

    if blockid == 1121: # double slab
        return self.build_block(top, side)

    # cut the side texture in half
    mask = side.crop((0,8,16,16))
    side = Image.new(side.mode, side.size, self.bgcolor)
    alpha_over(side, mask,(0,0,16,8), mask)

    # plain slab
    top = self.transform_image_top(top)
    side = self.transform_image_side(side)
    otherside = side.transpose(Image.FLIP_LEFT_RIGHT)

    sidealpha = side.split()[3]
    side = ImageEnhance.Brightness(side).enhance(0.9)
    side.putalpha(sidealpha)
    othersidealpha = otherside.split()[3]
    otherside = ImageEnhance.Brightness(otherside).enhance(0.8)
    otherside.putalpha(othersidealpha)

    # upside down slab
    delta = 0
    if data & 8 == 8:
        delta = 6

    img = Image.new("RGBA", (24,24), self.bgcolor)
    alpha_over(img, side, (0,12 - delta), side)
    alpha_over(img, otherside, (12,12 - delta), otherside)
    alpha_over(img, top, (0,6 - delta), top)

    return img


# escaliers
@material(blockid=[1011,1012,1014,1015,1058,1059,1060,1061,1062,1067,1068,1069,1070,1071,1072,1073,1074,1093,1094,1095,1096,1097,1098,1099,1100,1101,1102,1103,1115,1116], data=range(128), transparent=True, solid=True, nospawn=True)
def stairs(self, blockid, data):
    # preserve the upside-down bit
    upside_down = data & 0x4

    # find solid quarters within the top or bottom half of the block
    # NW NE SE SW
    quarters = [data & 0x8, data & 0x10, data & 0x20, data & 0x40]

    # rotate the quarters so we can pretend northdirection is always upper-left
    numpy.roll(quarters, [0,1,3,2][self.rotation])
    nw,ne,se,sw = quarters

    if blockid == 1011: # paille
        texture = self.load_image_texture("assets/minecraft/textures/blocks/paille.png").copy()
    elif blockid == 1012: # paille
        texture = self.load_image_texture("assets/minecraft/textures/blocks/paille.png").copy()
    elif blockid == 1014: # ardoise
        texture = self.load_image_texture("assets/minecraft/textures/blocks/ardoise.png").copy()
    elif blockid == 1015: # ardoise
        texture = self.load_image_texture("assets/minecraft/textures/blocks/ardoise.png").copy()
    elif blockid == 1058 or blockid == 1067: # pin
        texture = self.load_image_texture("assets/minecraft/textures/blocks/wood.png").copy()
    elif blockid == 1059 or blockid == 1068: # bouleau
        texture = self.load_image_texture("assets/minecraft/textures/blocks/wood_birch.png").copy()
    elif blockid == 1060 or blockid == 1069: # jungle
        texture = self.load_image_texture("assets/minecraft/textures/blocks/wood_jungle.png").copy()
    elif blockid == 1061 or blockid == 1070: # stone
        texture = self.load_image_texture("assets/minecraft/textures/blocks/stone.png").copy()
    elif blockid == 1062 or blockid == 1071: # sandstone
        texture = self.load_image_texture("assets/minecraft/textures/blocks/sandstone_top.png").copy()
    elif blockid == 1072:
        texture = self.load_image_texture("assets/minecraft/textures/blocks/nether_brick.png").copy()
    elif blockid == 1073:
        texture = self.load_image_texture("assets/minecraft/textures/blocks/brick.png").copy()
    elif blockid == 1074:
        texture = self.load_image_texture("assets/minecraft/textures/blocks/stonebricksmooth.png").copy()
    elif blockid == 1093:
        texture = self.load_image_texture("assets/minecraft/textures/blocks/brique-cuivre.png").copy()
    elif blockid == 1094:
        texture = self.load_image_texture("assets/minecraft/textures/blocks/brique-cuivre-oxyde.png").copy()
    elif blockid == 1095:
        texture = self.load_image_texture("assets/minecraft/textures/blocks/brique-cuivre-tres-oxyde.png").copy()
    elif blockid == 1096:
        texture = self.load_image_texture("assets/minecraft/textures/blocks/brique-acier.png").copy()
    elif blockid == 1097:
        texture = self.load_image_texture("assets/minecraft/textures/blocks/structure-cuivre.png").copy()
    elif blockid == 1098:
        texture = self.load_image_texture("assets/minecraft/textures/blocks/grille-acier.png").copy()
    elif blockid == 1099:
        texture = self.load_image_texture("assets/minecraft/textures/blocks/structure-acier.png").copy()
    elif blockid == 1100:
        texture = self.load_image_texture("assets/minecraft/textures/blocks/bloc-cuivre.png").copy()
    elif blockid == 1101:
        texture = self.load_image_texture("assets/minecraft/textures/blocks/nether_brick.png").copy()
    elif blockid == 1102:
        texture = self.load_image_texture("assets/minecraft/textures/blocks/bloc-cuivre-tres-oxyde.png").copy()
    elif blockid == 1103:
        texture = self.load_image_texture("assets/minecraft/textures/blocks/bloc-acier.png").copy()
    elif blockid == 1115:
        texture = self.load_image_texture("assets/minecraft/textures/blocks/tuile.png").copy()
    elif blockid == 1116:
        texture = self.load_image_texture("assets/minecraft/textures/blocks/tuile.png").copy()

    outside_l = texture.copy()
    outside_r = texture.copy()
    inside_l = texture.copy()
    inside_r = texture.copy()

    slab_top = texture.copy()

    push = 8 if upside_down else 0

    def rect(tex,coords):
        ImageDraw.Draw(tex).rectangle(coords,outline=(0,0,0,0),fill=(0,0,0,0))

    # cut out top or bottom half from inner surfaces
    rect(inside_l, (0,8-push,15,15-push))
    rect(inside_r, (0,8-push,15,15-push))

    # cut out missing or obstructed quarters from each surface
    if not nw:
        rect(outside_l, (0,push,7,7+push))
        rect(texture, (0,0,7,7))
    if not nw or sw:
        rect(inside_r, (8,push,15,7+push)) # will be flipped
    if not ne:
        rect(texture, (8,0,15,7))
    if not ne or nw:
        rect(inside_l, (0,push,7,7+push))
    if not ne or se:
        rect(inside_r, (0,push,7,7+push)) # will be flipped
    if not se:
        rect(outside_r, (0,push,7,7+push)) # will be flipped
        rect(texture, (8,8,15,15))
    if not se or sw:
        rect(inside_l, (8,push,15,7+push))
    if not sw:
        rect(outside_l, (8,push,15,7+push))
        rect(outside_r, (8,push,15,7+push)) # will be flipped
        rect(texture, (0,8,7,15))

    img = Image.new("RGBA", (24,24), self.bgcolor)

    if upside_down:
        # top should have no cut-outs after all
        texture = slab_top
    else:
        # render the slab-level surface
        slab_top = self.transform_image_top(slab_top)
        alpha_over(img, slab_top, (0,6))

    # render inner left surface
    inside_l = self.transform_image_side(inside_l)
    # Darken the vertical part of the second step
    sidealpha = inside_l.split()[3]
    # darken it a bit more than usual, looks better
    inside_l = ImageEnhance.Brightness(inside_l).enhance(0.8)
    inside_l.putalpha(sidealpha)
    alpha_over(img, inside_l, (6,3))

    # render inner right surface
    inside_r = self.transform_image_side(inside_r).transpose(Image.FLIP_LEFT_RIGHT)
    # Darken the vertical part of the second step
    sidealpha = inside_r.split()[3]
    # darken it a bit more than usual, looks better
    inside_r = ImageEnhance.Brightness(inside_r).enhance(0.7)
    inside_r.putalpha(sidealpha)
    alpha_over(img, inside_r, (6,3))

    # render outer surfaces
    alpha_over(img, self.build_full_block(texture, None, None, outside_l, outside_r))

    return img

# nether and normal fences
# uses pseudo-ancildata found in iterate.c
@material(blockid=[1020, 1127, 1128, 1153, 1154, 1155], data=range(256), transparent=True, nospawn=True)
def iron_fence(self, blockid, data):
    # no need for rotations, it uses pseudo data.
    # create needed images for Big stick fence

    blockdata = data & 0xf
    data = data >> 4

    if blockid == 1020:
        fence_top = self.load_image_texture("assets/minecraft/textures/blocks/blockIron.png").copy()
        fence_side = self.load_image_texture("assets/minecraft/textures/blocks/blockIron.png").copy()
        fence_small_side = self.load_image_texture("assets/minecraft/textures/blocks/blockIron.png").copy()
    elif blockid == 1127:
        if blockdata == 0:
            fence_top = self.load_image_texture("assets/minecraft/textures/blocks/bloc-cuivre.png").copy()
            fence_side = self.load_image_texture("assets/minecraft/textures/blocks/bloc-cuivre.png").copy()
            fence_small_side = self.load_image_texture("assets/minecraft/textures/blocks/bloc-cuivre.png").copy()
        elif blockdata == 2:
            fence_top = self.load_image_texture("assets/minecraft/textures/blocks/bloc-cuivre-oxyde.png").copy()
            fence_side = self.load_image_texture("assets/minecraft/textures/blocks/bloc-cuivre-oxyde.png").copy()
            fence_small_side = self.load_image_texture("assets/minecraft/textures/blocks/bloc-cuivre-oxyde.png").copy()
        else:
            fence_top = self.load_image_texture("assets/minecraft/textures/blocks/bloc-cuivre-tres-oxyde.png").copy()
            fence_side = self.load_image_texture("assets/minecraft/textures/blocks/bloc-cuivre-tres-oxyde.png").copy()
            fence_small_side = self.load_image_texture("assets/minecraft/textures/blocks/bloc-cuivre-tres-oxyde.png").copy()
    elif blockid == 1128:
        fence_top = self.load_image_texture("assets/minecraft/textures/blocks/bloc-acier.png").copy()
        fence_side = self.load_image_texture("assets/minecraft/textures/blocks/bloc-acier.png").copy()
        fence_small_side = self.load_image_texture("assets/minecraft/textures/blocks/bloc-acier.png").copy()
    elif blockid == 1153:
        fence_top = self.load_image_texture("assets/minecraft/textures/blocks/planks_spruce.png").copy()
        fence_side = self.load_image_texture("assets/minecraft/textures/blocks/planks_spruce.png").copy()
        fence_small_side = self.load_image_texture("assets/minecraft/textures/blocks/planks_spruce.png").copy()
    elif blockid == 1154:
        fence_top = self.load_image_texture("assets/minecraft/textures/blocks/planks_birch.png").copy()
        fence_side = self.load_image_texture("assets/minecraft/textures/blocks/planks_birch.png").copy()
        fence_small_side = self.load_image_texture("assets/minecraft/textures/blocks/planks_birch.png").copy()
    elif blockid == 1155:
        fence_top = self.load_image_texture("assets/minecraft/textures/blocks/planks_jungle.png").copy()
        fence_side = self.load_image_texture("assets/minecraft/textures/blocks/planks_jungle.png").copy()
        fence_small_side = self.load_image_texture("assets/minecraft/textures/blocks/planks_jungle.png").copy()

    # generate the textures of the fence
    ImageDraw.Draw(fence_top).rectangle((0,0,5,15),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(fence_top).rectangle((10,0,15,15),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(fence_top).rectangle((0,0,15,5),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(fence_top).rectangle((0,10,15,15),outline=(0,0,0,0),fill=(0,0,0,0))

    ImageDraw.Draw(fence_side).rectangle((0,0,5,15),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(fence_side).rectangle((10,0,15,15),outline=(0,0,0,0),fill=(0,0,0,0))

    # Create the sides and the top of the big stick
    fence_side = self.transform_image_side(fence_side)
    fence_other_side = fence_side.transpose(Image.FLIP_LEFT_RIGHT)
    fence_top = self.transform_image_top(fence_top)

    # Darken the sides slightly. These methods also affect the alpha layer,
    # so save them first (we don't want to "darken" the alpha layer making
    # the block transparent)
    sidealpha = fence_side.split()[3]
    fence_side = ImageEnhance.Brightness(fence_side).enhance(0.9)
    fence_side.putalpha(sidealpha)
    othersidealpha = fence_other_side.split()[3]
    fence_other_side = ImageEnhance.Brightness(fence_other_side).enhance(0.8)
    fence_other_side.putalpha(othersidealpha)

    # Compose the fence big stick
    fence_big = Image.new("RGBA", (24,24), self.bgcolor)
    alpha_over(fence_big,fence_side, (5,4),fence_side)
    alpha_over(fence_big,fence_other_side, (7,4),fence_other_side)
    alpha_over(fence_big,fence_top, (0,0),fence_top)

    # Now render the small sticks.
    # Create needed images
    ImageDraw.Draw(fence_small_side).rectangle((0,0,15,0),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(fence_small_side).rectangle((0,4,15,6),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(fence_small_side).rectangle((0,10,15,16),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(fence_small_side).rectangle((0,0,4,15),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(fence_small_side).rectangle((11,0,15,15),outline=(0,0,0,0),fill=(0,0,0,0))

    # Create the sides and the top of the small sticks
    fence_small_side = self.transform_image_side(fence_small_side)
    fence_small_other_side = fence_small_side.transpose(Image.FLIP_LEFT_RIGHT)

    # Darken the sides slightly. These methods also affect the alpha layer,
    # so save them first (we don't want to "darken" the alpha layer making
    # the block transparent)
    sidealpha = fence_small_other_side.split()[3]
    fence_small_other_side = ImageEnhance.Brightness(fence_small_other_side).enhance(0.9)
    fence_small_other_side.putalpha(sidealpha)
    sidealpha = fence_small_side.split()[3]
    fence_small_side = ImageEnhance.Brightness(fence_small_side).enhance(0.9)
    fence_small_side.putalpha(sidealpha)

    # Create img to compose the fence
    img = Image.new("RGBA", (24,24), self.bgcolor)

    # Position of fence small sticks in img.
    # These postitions are strange because the small sticks of the
    # fence are at the very left and at the very right of the 16x16 images
    pos_top_left = (2,3)
    pos_top_right = (10,3)
    pos_bottom_right = (10,7)
    pos_bottom_left = (2,7)

    # +x axis points top right direction
    # +y axis points bottom right direction
    # First compose small sticks in the back of the image,
    # then big stick and thecn small sticks in the front.

    if (data & 0b0001) == 1:
        alpha_over(img,fence_small_side, pos_top_left,fence_small_side)                # top left
    if (data & 0b1000) == 8:
        alpha_over(img,fence_small_other_side, pos_top_right,fence_small_other_side)    # top right

    alpha_over(img,fence_big,(0,0),fence_big)

    if (data & 0b0010) == 2:
        alpha_over(img,fence_small_other_side, pos_bottom_left,fence_small_other_side)      # bottom left
    if (data & 0b0100) == 4:
        alpha_over(img,fence_small_side, pos_bottom_right,fence_small_side)                  # bottom right

    return img

# couvert
@material(blockid=1024,  data=range(4), transparent=True, solid=True)
def couvert(self, blockid, data):
    top = self.load_image_texture("assets/minecraft/textures/blocks/assiette.png")
    if data == 0:
      top = top.copy().rotate(90)
    elif data == 3:
      top = top.copy().rotate(180)
    elif data == 2:
      top = top.copy().rotate(270)

    top = self.transform_image_top(top)

    img = Image.new("RGBA", (24,24), self.bgcolor)
    alpha_over(img, top, (0,12), top)

    return img

@material(blockid=1036, data=range(16), solid=True)
def lanterne_eteinte(self, blockid, data):
    t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-blanc.png")

    if (data) == 0:
        t = self.load_image_texture("assets/minecraft/textures/blocks/lanterne-glowstone-off.png")
    if (data) == 1:
        t = self.load_image_texture("assets/minecraft/textures/blocks/lanterne-glowstone-pure-off.png")

    return self.build_block(t, t)

@material(blockid=1038, data=range(16), solid=True)
def verreColore(self, blockid, data):
    t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-blanc.png")

    if (data) == 0:
            t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-orange.png")
    if (data) == 1:
            t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-blanc.png")
    if (data) == 2:
            t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-rouge.png")
    if (data) == 3:
            t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-noir.png")
    if (data) == 4:
            t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-vert.png")
    if (data) == 5:
            t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-brun.png")
    if (data) == 6:
            t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-bleu.png")
    if (data) == 7:
            t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-violet.png")
    if (data) == 8:
            t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-cyan.png")
    if (data) == 9:
            t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-gris-clair.png")
    if (data) == 10:
            t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-gris-fonce.png")
    if (data) == 11:
            t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-rose.png")
    if (data) == 12:
            t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-vert-clair.png")
    if (data) == 13:
            t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-jaune.png")
    if (data) == 14:
            t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-bleu-clair.png")
    if (data) == 15:
            t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-magenta.png")

    half_block_u = t.copy() # up, down, left, right
    half_block_d = t.copy()
    half_block_l = t.copy()
    half_block_r = t.copy()

    # generate needed geometries
    ImageDraw.Draw(half_block_u).rectangle((0,8,15,15),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(half_block_d).rectangle((0,0,15,6),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(half_block_l).rectangle((8,0,15,15),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(half_block_r).rectangle((0,0,7,15),outline=(0,0,0,0),fill=(0,0,0,0))

    return self.build_block(t, t)


# poulie
@material(blockid=1040, data=range(16))
def poulie(self, blockid, data):
    top = self.load_image_texture("assets/minecraft/textures/blocks/poulie-top.png")
    side = self.load_image_texture("assets/minecraft/textures/blocks/poulie-side.png")
    front = self.load_image_texture("assets/minecraft/textures/blocks/poulie-cote.png")

    return self.build_full_block(top, None, None, side, front)

# chaine
@material(blockid=1041, data=range(16), transparent=True)
def chain(self, blockid, data):
    tex = self.load_image_texture("assets/minecraft/textures/blocks/chaine-1.png")
    return self.build_sprite(tex)

# fireworks
@material(blockid=1043, top_index=110, side_index=109)
def fireworks(self, blockid, data):
    top = self.load_image_texture("assets/minecraft/textures/blocks/piston_inner_top.png")
    side = self.load_image_texture("assets/minecraft/textures/blocks/furnace_side.png")
    front = self.load_image_texture("assets/minecraft/textures/blocks/furnace_top.png")

    return self.build_full_block(top, None, None, side, front)

# verre colore
@material(blockid=1044, data=range(256), transparent=True, nospawn=True)
def panes(self, blockid, data):
    blockdata = data & 0xf;
    if (blockdata) == 0:
            t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-orange.png")
    if (blockdata) == 1:
            t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-blanc.png")
    if (blockdata) == 2:
            t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-rouge.png")
    if (blockdata) == 3:
            t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-noir.png")
    if (blockdata) == 4:
            t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-vert.png")
    if (blockdata) == 5:
            t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-brun.png")
    if (blockdata) == 6:
            t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-bleu.png")
    if (blockdata) == 7:
            t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-violet.png")
    if (blockdata) == 8:
            t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-cyan.png")
    if (blockdata) == 9:
            t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-gris-clair.png")
    if (blockdata) == 10:
            t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-gris-fonce.png")
    if (blockdata) == 11:
            t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-rose.png")
    if (blockdata) == 12:
            t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-vert-clair.png")
    if (blockdata) == 13:
            t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-jaune.png")
    if (blockdata) == 14:
            t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-bleu-clair.png")
    if (blockdata) == 15:
            t = self.load_image_texture("assets/minecraft/textures/blocks/verre-colore-magenta.png")

    left = t.copy()
    right = t.copy()

    # generate the four small pieces of the glass pane
    ImageDraw.Draw(right).rectangle((0,0,7,15),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(left).rectangle((8,0,15,15),outline=(0,0,0,0),fill=(0,0,0,0))

    up_left = self.transform_image_side(left)
    up_right = self.transform_image_side(right).transpose(Image.FLIP_TOP_BOTTOM)
    dw_right = self.transform_image_side(right)
    dw_left = self.transform_image_side(left).transpose(Image.FLIP_TOP_BOTTOM)

    # Create img to compose the texture
    img = Image.new("RGBA", (24,24), self.bgcolor)

    # +x axis points top right direction
    # +y axis points bottom right direction
    # First compose things in the back of the image,
    # then things in the front.

    # the lower 4 bits encode color, the upper 4 encode adjencies
    data = data >> 4

    if (data & 0b0001) == 1 or data == 0:
        alpha_over(img,up_left, (6,3),up_left)    # top left
    if (data & 0b1000) == 8 or data == 0:
        alpha_over(img,up_right, (6,3),up_right)  # top right
    if (data & 0b0010) == 2 or data == 0:
        alpha_over(img,dw_left, (6,3),dw_left)    # bottom left
    if (data & 0b0100) == 4 or data == 0:
        alpha_over(img,dw_right, (6,3),dw_right)  # bottom right

    return img

# portal
@material(blockid=1045, data=range(4), transparent=True)
def portal(self, blockid, data):
    # no rotations, uses pseudo data
    portaltexture = self.load_portal()
    img = Image.new("RGBA", (24,24), self.bgcolor)

    side = self.transform_image_side(portaltexture)
    otherside = side.transpose(Image.FLIP_TOP_BOTTOM)

    if data in (0,2):
        alpha_over(img, side, (5,4), side)

    if data in (1,3):
        alpha_over(img, otherside, (5,4), otherside)

    return img

# tronc couche
@material(blockid=1046, data=range(16), solid=True)
def troncCouche(self, blockid, data):
    orient = data & 0x8;
    woodtype = data & 0x7;
    if woodtype == 0:
        specialside = self.load_image_texture("assets/minecraft/textures/blocks/tree_side.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/tree_side.png")
    elif woodtype == 1:
        specialside = self.load_image_texture("assets/minecraft/textures/blocks/oak-side.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/oak-side.png")
    elif woodtype == 2:
        specialside = self.load_image_texture("assets/minecraft/textures/blocks/birch-side.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/birch-side.png")
    else:
        specialside = self.load_image_texture("assets/minecraft/textures/blocks/jungle-side.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/jungle-side.png")

    if orient == 0:
      return self.build_full_block(side, None, None, specialside, self.load_image_texture("assets/minecraft/textures/blocks/tree_top.png"))
    else:
      return self.build_full_block(specialside, None, None, self.load_image_texture("assets/minecraft/textures/blocks/tree_top.png"), specialside)

# vase
@material(blockid=[1049,1052,1054,1056,1135,1137,1139,1141,1143,1145,1147], data=range(16), transparent=True, solid=False, nospawn=True)
def vase(self, blockid, data):
    if blockid == 1049:
        top = self.load_image_texture("assets/minecraft/textures/blocks/vase-pierre-top.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/vase-pierre-side.png")
    elif blockid == 1052:
        top = self.load_image_texture("assets/minecraft/textures/blocks/vase-bois-top.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/vase-bois-side.png")
    elif blockid == 1054:
        top = self.load_image_texture("assets/minecraft/textures/blocks/vase-marbre-blanc-top.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/vase-marbre-blanc-side.png")
    elif blockid == 1056:
        top = self.load_image_texture("assets/minecraft/textures/blocks/vase-marbre-noir-top.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/vase-marbre-noir-side.png")
    elif blockid == 1135:
        top = self.load_image_texture("assets/minecraft/textures/blocks/vase-pine-top.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/vase-pine-side.png")
    elif blockid == 1137:
        top = self.load_image_texture("assets/minecraft/textures/blocks/vase-birch-top.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/vase-birch-side.png")
    elif blockid == 1139:
        top = self.load_image_texture("assets/minecraft/textures/blocks/vase-jungle-top.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/vase-jungle-side.png")
    elif blockid == 1141:
        top = self.load_image_texture("assets/minecraft/textures/blocks/vase-acier-top.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/vase-acier-side.png")
    elif blockid == 1143:
        top = self.load_image_texture("assets/minecraft/textures/blocks/vase-cuivre-top.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/vase-cuivre-side.png")
    elif blockid == 1145:
        top = self.load_image_texture("assets/minecraft/textures/blocks/vase-cuivresemi-top.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/vase-cuivresemi-side.png")
    elif blockid == 1147:
        top = self.load_image_texture("assets/minecraft/textures/blocks/vase-cuivreoxy-top.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/vase-cuivreoxy-side.png")

    img = Image.new("RGBA", (24,24), self.bgcolor)

    top = self.transform_image_top(top)
    side = self.transform_image_side(side)
    otherside = side.transpose(Image.FLIP_LEFT_RIGHT)

    sidealpha = side.split()[3]
    side = ImageEnhance.Brightness(side).enhance(0.9)
    side.putalpha(sidealpha)
    othersidealpha = otherside.split()[3]
    otherside = ImageEnhance.Brightness(otherside).enhance(0.8)
    otherside.putalpha(othersidealpha)

    alpha_over(img, side, (3,6), side)
    alpha_over(img, otherside, (10,6), otherside)
    alpha_over(img, top, (0,0), top)

    return img

# fleur
@material(blockid=1050, data=range(16), transparent=True, nospawn=True)
def flowers(self, blockid, data):
    t = self.load_image_texture("assets/minecraft/textures/blocks/fleur-aspidistra.png")

    if (data) == 0:
        t = self.load_image_texture("assets/minecraft/textures/blocks/fleur-aspidistra.png")
    if (data) == 1:
        t = self.load_image_texture("assets/minecraft/textures/blocks/fleur-edelweiss.png")
    if (data) == 2:
        t = self.load_image_texture("assets/minecraft/textures/blocks/fleur-polypodi.png")
    if (data) == 3:
        t = self.load_image_texture("assets/minecraft/textures/blocks/fleur-dipteris.png")
    if (data) == 4:
        t = self.load_image_texture("assets/minecraft/textures/blocks/fleur-ptedirium.png")
    if (data) == 5:
        t = self.load_image_texture("assets/minecraft/textures/blocks/fleur-gentiane.png")
    if (data) == 6:
        t = self.load_image_texture("assets/minecraft/textures/blocks/fleur-ipomee.png")
    if (data) == 7:
        t = self.load_image_texture("assets/minecraft/textures/blocks/fleur-jacinthe.png")
    if (data) == 8:
        t = self.load_image_texture("assets/minecraft/textures/blocks/fleur-martagon.png")
    if (data) == 9:
        t = self.load_image_texture("assets/minecraft/textures/blocks/fleur-muguet.png")
    if (data) == 10:
        t = self.load_image_texture("assets/minecraft/textures/blocks/fleur-primevere.png")
    if (data) == 11:
        t = self.load_image_texture("assets/minecraft/textures/blocks/fleur-rose.png")
    if (data) == 12:
        t = self.load_image_texture("assets/minecraft/textures/blocks/fleur-tulipe.png")
    if (data) == 13:
        t = self.load_image_texture("assets/minecraft/textures/blocks/fleur-arum.png")

    return self.build_sprite(t)

# marbre blanc / noir (+ raffiné)
@material(blockid=1063, data=range(16), solid=True)
def marbre(self, blockid, data):
  if data == 6:
    top = self.load_image_texture("assets/minecraft/textures/blocks/marbre-white-halfstep-top.png")
    side = self.load_image_texture("assets/minecraft/textures/blocks/marbre-white-halfstep-top.png")
  elif data == 7:
    top = self.load_image_texture("assets/minecraft/textures/blocks/marbre-noir-halfstep-top.png")
    side = self.load_image_texture("assets/minecraft/textures/blocks/marbre-noir-halfstep-top.png")
  elif data == 8:
    top = self.load_image_texture("assets/minecraft/textures/blocks/marbre-blanc-brut.png")
    side = self.load_image_texture("assets/minecraft/textures/blocks/marbre-blanc-brut.png")
  elif data == 9:
    top = self.load_image_texture("assets/minecraft/textures/blocks/marbre-noir-brut.png")
    side = self.load_image_texture("assets/minecraft/textures/blocks/marbre-noir-brut.png")
  else:
      return None

  return self.build_block(top, side)

# metro
@material(blockid=1064, data=range(16), solid=True)
def metro(self, blockid, data):
    t = self.load_image_texture("assets/minecraft/textures/blocks/tram-a.png")

    if (data) == 0:
        t = self.load_image_texture("assets/minecraft/textures/blocks/tram-a.png")
    if (data) == 1:
        t = self.load_image_texture("assets/minecraft/textures/blocks/tram-b.png")
    if (data) == 2:
        t = self.load_image_texture("assets/minecraft/textures/blocks/tram-c.png")
    if (data) == 3:
        t = self.load_image_texture("assets/minecraft/textures/blocks/tram-d.png")
    if (data) == 4:
        t = self.load_image_texture("assets/minecraft/textures/blocks/tram-e.png")
    if (data) == 5:
        t = self.load_image_texture("assets/minecraft/textures/blocks/tram-f.png")

    return self.build_block(t, t)

# joyaux
@material(blockid=1066, data=range(16), solid=True)
def joyaux(self, blockid, data):
    t = self.load_image_texture("assets/minecraft/textures/blocks/jade-block.png")

    if (data) == 0:
        t = self.load_image_texture("assets/minecraft/textures/blocks/jade-block.png")
    if (data) == 1:
        t = self.load_image_texture("assets/minecraft/textures/blocks/ruby-block.png")
    if (data) == 2:
        t = self.load_image_texture("assets/minecraft/textures/blocks/citrine-block.png")
    if (data) == 3:
        t = self.load_image_texture("assets/minecraft/textures/blocks/saphire-block.png")
    if (data) == 4:
        t = self.load_image_texture("assets/minecraft/textures/blocks/jade-block-pure.png")
    if (data) == 5:
        t = self.load_image_texture("assets/minecraft/textures/blocks/ruby-block-pure.png")
    if (data) == 6:
        t = self.load_image_texture("assets/minecraft/textures/blocks/citrine-block-pure.png")
    if (data) == 7:
        t = self.load_image_texture("assets/minecraft/textures/blocks/saphire-block-pure.png")

    return self.build_block(t, t)

# stone brick taille
@material(blockid=1083, data=range(16), solid=True)
def stone_brick_taille(self, blockid, data):
    t = self.load_image_texture("assets/minecraft/textures/blocks/pierre-taillee-1.png")

    if (data) == 0:
        t = self.load_image_texture("assets/minecraft/textures/blocks/pierre-taillee-1.png")
    if (data) == 1:
        t = self.load_image_texture("assets/minecraft/textures/blocks/pierre-taillee-2.png")
    if (data) == 2:
        t = self.load_image_texture("assets/minecraft/textures/blocks/pierre-taillee-3.png")
    if (data) == 3:
        t = self.load_image_texture("assets/minecraft/textures/blocks/pierre-taillee-4.png")
    if (data) == 4:
        t = self.load_image_texture("assets/minecraft/textures/blocks/pierre-taillee-5.png")
    if (data) == 5:
        t = self.load_image_texture("assets/minecraft/textures/blocks/pierre-taillee-6.png")
    if (data) == 6:
        t = self.load_image_texture("assets/minecraft/textures/blocks/pierre-taillee-7.png")
    if (data) == 7:
        t = self.load_image_texture("assets/minecraft/textures/blocks/pierre-taillee-8.png")
    if (data) == 8:
        t = self.load_image_texture("assets/minecraft/textures/blocks/pierre-taillee-9.png")
    if (data) == 9:
        t = self.load_image_texture("assets/minecraft/textures/blocks/pierre-taillee-10.png")
    if (data) == 10:
        t = self.load_image_texture("assets/minecraft/textures/blocks/pierre-taillee-11.png")
    if (data) == 11:
        t = self.load_image_texture("assets/minecraft/textures/blocks/pierre-taillee-12.png")

    return self.build_block(t, t)

# stone brick joyaux
@material(blockid=1084, data=range(16), solid=True)
def stone_brick_incr(self, blockid, data):
    t = self.load_image_texture("assets/minecraft/textures/blocks/jade-block.png")

    if (data) == 0:
        t = self.load_image_texture("assets/minecraft/textures/blocks/pierre-taillee-12.png")
    if (data) == 1:
        t = self.load_image_texture("assets/minecraft/textures/blocks/pierre-sertie-jade-1.png")
    if (data) == 2:
        t = self.load_image_texture("assets/minecraft/textures/blocks/pierre-sertie-rubis-1.png")
    if (data) == 3:
        t = self.load_image_texture("assets/minecraft/textures/blocks/pierre-sertie-citrine-1.png")
    if (data) == 4:
        t = self.load_image_texture("assets/minecraft/textures/blocks/pierre-sertie-saphire-1.png")
    if (data) == 5:
        t = self.load_image_texture("assets/minecraft/textures/blocks/pierre-sertissable-2.png")
    if (data) == 6:
        t = self.load_image_texture("assets/minecraft/textures/blocks/pierre-sertie-jade-2.png")
    if (data) == 7:
        t = self.load_image_texture("assets/minecraft/textures/blocks/pierre-sertie-rubis-2.png")
    if (data) == 8:
        t = self.load_image_texture("assets/minecraft/textures/blocks/pierre-sertie-citrine-2.png")
    if (data) == 9:
        t = self.load_image_texture("assets/minecraft/textures/blocks/pierre-sertie-saphire-2.png")
    if (data) == 10:
        t = self.load_image_texture("assets/minecraft/textures/blocks/pierre-sertissable-3.png")
    if (data) == 11:
        t = self.load_image_texture("assets/minecraft/textures/blocks/pierre-sertie-jade-3.png")

    return self.build_block(t, t)

# lampion
@material(blockid=[1085], data=range(16), transparent=True)
def lampion(self, blockid, data):
    t = self.load_image_texture("assets/minecraft/textures/blocks/fleur-aspidistra.png")

    if (data) == 0:
        t = self.load_image_texture("assets/minecraft/textures/blocks/lampion-blanc.png")
    if (data) == 1:
        t = self.load_image_texture("assets/minecraft/textures/blocks/lampion-gris-clair.png")
    if (data) == 2:
        t = self.load_image_texture("assets/minecraft/textures/blocks/lampion-gris.png")
    if (data) == 3:
        t = self.load_image_texture("assets/minecraft/textures/blocks/lampion-noir.png")
    if (data) == 4:
        t = self.load_image_texture("assets/minecraft/textures/blocks/lampion-bleu-clair.png")
    if (data) == 5:
        t = self.load_image_texture("assets/minecraft/textures/blocks/lampion-cyan.png")
    if (data) == 6:
        t = self.load_image_texture("assets/minecraft/textures/blocks/lampion-bleu.png")
    if (data) == 7:
        t = self.load_image_texture("assets/minecraft/textures/blocks/lampion-violet.png")
    if (data) == 8:
        t = self.load_image_texture("assets/minecraft/textures/blocks/lampion-magenta.png")
    if (data) == 9:
        t = self.load_image_texture("assets/minecraft/textures/blocks/lampion-rose.png")
    if (data) == 10:
        t = self.load_image_texture("assets/minecraft/textures/blocks/lampion-jaune.png")
    if (data) == 11:
        t = self.load_image_texture("assets/minecraft/textures/blocks/lampion-orange.png")
    if (data) == 12:
        t = self.load_image_texture("assets/minecraft/textures/blocks/lampion-marron.png")
    if (data) == 13:
        t = self.load_image_texture("assets/minecraft/textures/blocks/lampion-rouge.png")
    if (data) == 14:
        t = self.load_image_texture("assets/minecraft/textures/blocks/lampion-vert-clair.png")
    if (data) == 15:
        t = self.load_image_texture("assets/minecraft/textures/blocks/lampion-vert.png")

    top = t.copy()
    tmp_top = t.copy()
    ImageDraw.Draw(tmp_top).rectangle((0,0,8,16),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(tmp_top).rectangle((8,0,16,8),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(top).rectangle((0,0,16,16),outline=(0,0,0,0),fill=(0,0,0,0))
    top.paste(tmp_top,(-4,-4,12,12))

    side = t.copy()
    tmp = t.copy()
    ImageDraw.Draw(tmp).rectangle((8,0,16,16),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(side).rectangle((0,0,16,16),outline=(0,0,0,0),fill=(0,0,0,0))
    side.paste(tmp,(4,0,20,16))

    img = Image.new("RGBA", (24,24), self.bgcolor)

    top = self.transform_image_top(top)
    side = self.transform_image_side(side)
    otherside = side.transpose(Image.FLIP_LEFT_RIGHT)

    sidealpha = side.split()[3]
    side = ImageEnhance.Brightness(side).enhance(0.9)
    side.putalpha(sidealpha)
    othersidealpha = otherside.split()[3]
    otherside = ImageEnhance.Brightness(otherside).enhance(0.8)
    otherside.putalpha(othersidealpha)

    alpha_over(img, side, (4,6), side)
    alpha_over(img, otherside, (9,6), otherside)
    alpha_over(img, top, (0,1), top)

    return img

# bloc (+ brique) cuivre
@material(blockid=1088, data=range(6), solid=True)
def bloqueBriqueCuivre(self, blockid, data):
    t = self.load_image_texture("assets/minecraft/textures/blocks/bloc-cuivre.png")

    if (data) == 0:
        t = self.load_image_texture("assets/minecraft/textures/blocks/bloc-cuivre.png")
    if (data) == 1:
        t = self.load_image_texture("assets/minecraft/textures/blocks/brique-cuivre.png")
    if (data) == 2:
        t = self.load_image_texture("assets/minecraft/textures/blocks/bloc-cuivre-oxyde.png")
    if (data) == 3:
        t = self.load_image_texture("assets/minecraft/textures/blocks/brique-cuivre-oxyde.png")
    if (data) == 4:
        t = self.load_image_texture("assets/minecraft/textures/blocks/bloc-cuivre-tres-oxyde.png")
    if (data) == 5:
        t = self.load_image_texture("assets/minecraft/textures/blocks/brique-cuivre-tres-oxyde.png")

    return self.build_block(t, t)

# bloc (+ brique) acier
@material(blockid=1089, data=range(2), solid=True)
def blocBriqueAcier(self, blockid, data):
    t = self.load_image_texture("assets/minecraft/textures/blocks/bloc-cuivre.png")

    if (data) == 0:
        t = self.load_image_texture("assets/minecraft/textures/blocks/bloc-acier.png")
    if (data) == 1:
        t = self.load_image_texture("assets/minecraft/textures/blocks/brique-acier.png")

    return self.build_block(t, t)

# demi dalle (acier | cuivre)
@material(blockid=[1090], data=range(16), transparent=True, solid=True)
def slabs(self, blockid, data):
    top = self.load_image_texture("assets/minecraft/textures/blocks/bloc-cuivre.png")
    side = self.load_image_texture("assets/minecraft/textures/blocks/bloc-cuivre.png")

    if (data) == 0:
        top = self.load_image_texture("assets/minecraft/textures/blocks/brique-cuivre.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/brique-cuivre.png")
    elif (data) == 1:
        top = self.load_image_texture("assets/minecraft/textures/blocks/brique-cuivre-oxyde.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/brique-cuivre-oxyde.png")
    elif (data) == 3:
        top = self.load_image_texture("assets/minecraft/textures/blocks/brique-acier.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/brique-acier.png")
    elif (data) == 4:
        top = self.load_image_texture("assets/minecraft/textures/blocks/bloc-acier.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/bloc-acier.png")
    elif (data) == 5:
        top = self.load_image_texture("assets/minecraft/textures/blocks/bloc-cuivre.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/bloc-cuivre.png")
    elif (data) == 6:
        top = self.load_image_texture("assets/minecraft/textures/blocks/bloc-cuivre-oxyde.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/bloc-cuivre-oxyde.png")
    elif (data) == 7:
        top = self.load_image_texture("assets/minecraft/textures/blocks/bloc-cuivre-tres-oxyde.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/bloc-cuivre-tres-oxyde.png")
    else:
        return None

    # cut the side texture in half
    mask = side.crop((0,8,16,16))
    side = Image.new(side.mode, side.size, self.bgcolor)
    alpha_over(side, mask,(0,0,16,8), mask)

    # plain slab
    top = self.transform_image_top(top)
    side = self.transform_image_side(side)
    otherside = side.transpose(Image.FLIP_LEFT_RIGHT)

    sidealpha = side.split()[3]
    side = ImageEnhance.Brightness(side).enhance(0.9)
    side.putalpha(sidealpha)
    othersidealpha = otherside.split()[3]
    otherside = ImageEnhance.Brightness(otherside).enhance(0.8)
    otherside.putalpha(othersidealpha)

    # upside down slab
    delta = 0
    if data & 8 == 8:
        delta = 6

    img = Image.new("RGBA", (24,24), self.bgcolor)
    alpha_over(img, side, (0,12 - delta), side)
    alpha_over(img, otherside, (12,12 - delta), otherside)
    alpha_over(img, top, (0,6 - delta), top)

    return img


# trapdoor

# TODO
# the trapdoor looks like a sprite when opened, that's not good
@material(blockid=1091, data=range(8), transparent=True, nospawn=True)
def trapdoor(self, blockid, data):

    # rotation
    # Masked to not clobber opened/closed info
    if self.rotation == 1:
        if (data & 0b0011) == 0: data = data & 0b1100 | 3
        elif (data & 0b0011) == 1: data = data & 0b1100 | 2
        elif (data & 0b0011) == 2: data = data & 0b1100 | 0
        elif (data & 0b0011) == 3: data = data & 0b1100 | 1
    elif self.rotation == 2:
        if (data & 0b0011) == 0: data = data & 0b1100 | 1
        elif (data & 0b0011) == 1: data = data & 0b1100 | 0
        elif (data & 0b0011) == 2: data = data & 0b1100 | 3
        elif (data & 0b0011) == 3: data = data & 0b1100 | 2
    elif self.rotation == 3:
        if (data & 0b0011) == 0: data = data & 0b1100 | 2
        elif (data & 0b0011) == 1: data = data & 0b1100 | 3
        elif (data & 0b0011) == 2: data = data & 0b1100 | 1
        elif (data & 0b0011) == 3: data = data & 0b1100 | 0

    # texture generation
    texture = self.load_image_texture("assets/minecraft/textures/blocks/trapdoor.png")
    if data & 0x4 == 0x4: # opened trapdoor
        if data & 0x3 == 0: # west
            img = self.build_full_block(None, None, None, None, texture)
        if data & 0x3 == 1: # east
            img = self.build_full_block(None, texture, None, None, None)
        if data & 0x3 == 2: # south
            img = self.build_full_block(None, None, texture, None, None)
        if data & 0x3 == 3: # north
            img = self.build_full_block(None, None, None, texture, None)

    elif data & 0x4 == 0: # closed trapdoor
        img = self.build_full_block((texture, 12), None, None, texture, texture)

    return img

# structure
@material(blockid=1092, data=range(3), transparent=True, nospawn=True)
def structure(self, blockid, data):
    t = self.load_image_texture("assets/minecraft/textures/blocks/jade-block.png")

    if (data) == 0:
	t = self.load_image_texture("assets/minecraft/textures/blocks/structure-cuivre.png")
    if (data) == 1:
        t = self.load_image_texture("assets/minecraft/textures/blocks/grille-acier.png")
    if (data) == 2:
        t = self.load_image_texture("assets/minecraft/textures/blocks/structure-acier.png")

    return self.build_block(t, t)

# algue
@material(blockid=1105, data=range(16), transparent=True)
def algue(self, blockid, data):
    tex = self.load_image_texture("assets/minecraft/textures/blocks/algue.png")
    return self.build_sprite(tex)

# dirt with algue
@material(blockid=1106, data=range(16), solid=True)
def dirtWithAlgue(self, blockid, data):
    texture = self.load_image_texture("assets/minecraft/textures/blocks/dirt-recouvert-algue.png")
    return self.build_block(texture, texture)

# sand with algue
@material(blockid=1107, data=range(16), solid=True)
def dirtWithAlgue(self, blockid, data):
    texture = self.load_image_texture("assets/minecraft/textures/blocks/sable-recouvert-algue.png")
    return self.build_block(texture, texture)

# coraux
@material(blockid=1108, data=range(16), transparent=True)
def coraux(self, blockid, data):
    t = self.load_image_texture("assets/minecraft/textures/blocks/jade-block.png")

    if (data) == 0:
        t = self.load_image_texture("assets/minecraft/textures/blocks/corail-blanc.png")
    if (data) == 1:
        t = self.load_image_texture("assets/minecraft/textures/blocks/corail-gris-clair.png")
    if (data) == 2:
        t = self.load_image_texture("assets/minecraft/textures/blocks/corail-gris.png")
    if (data) == 3:
        t = self.load_image_texture("assets/minecraft/textures/blocks/corail-noir.png")
    if (data) == 4:
        t = self.load_image_texture("assets/minecraft/textures/blocks/corail-bleu-clair.png")
    if (data) == 5:
        t = self.load_image_texture("assets/minecraft/textures/blocks/corail-cyan.png")
    if (data) == 6:
        t = self.load_image_texture("assets/minecraft/textures/blocks/corail-bleu.png")
    if (data) == 7:
        t = self.load_image_texture("assets/minecraft/textures/blocks/corail-violet.png")
    if (data) == 8:
        t = self.load_image_texture("assets/minecraft/textures/blocks/corail-magenta.png")
    if (data) == 9:
        t = self.load_image_texture("assets/minecraft/textures/blocks/corail-rose.png")
    if (data) == 10:
        t = self.load_image_texture("assets/minecraft/textures/blocks/corail-jaune.png")
    if (data) == 11:
        t = self.load_image_texture("assets/minecraft/textures/blocks/corail-orange.png")
    if (data) == 12:
        t = self.load_image_texture("assets/minecraft/textures/blocks/corail-marron.png")
    if (data) == 13:
        t = self.load_image_texture("assets/minecraft/textures/blocks/corail-rouge.png")
    if (data) == 14:
        t = self.load_image_texture("assets/minecraft/textures/blocks/corail-vert-clair.png")
    if (data) == 15:
        t = self.load_image_texture("assets/minecraft/textures/blocks/corail-vert.png")

    return self.build_sprite(t)

# Tuile
block(blockid=1114, top_image="assets/minecraft/textures/blocks/tuile.png")

# dalle en tuile
@material(blockid=[1017,1120], data=range(16), transparent=True, solid=True)
def slabs(self, blockid, data):
    texture = data & 0x7;
    if texture== 0: # tuile
        top = self.load_image_texture("assets/minecraft/textures/blocks/tuile.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/tuile.png")
    else:
        return None

    if blockid == 1120: # double slab
        return self.build_block(top, side)

    # cut the side texture in half
    mask = side.crop((0,8,16,16))
    side = Image.new(side.mode, side.size, self.bgcolor)
    alpha_over(side, mask,(0,0,16,8), mask)

    # plain slab
    top = self.transform_image_top(top)
    side = self.transform_image_side(side)
    otherside = side.transpose(Image.FLIP_LEFT_RIGHT)

    sidealpha = side.split()[3]
    side = ImageEnhance.Brightness(side).enhance(0.9)
    side.putalpha(sidealpha)
    othersidealpha = otherside.split()[3]
    otherside = ImageEnhance.Brightness(otherside).enhance(0.8)
    otherside.putalpha(othersidealpha)

    # upside down slab
    delta = 0
    if data & 8 == 8:
        delta = 6

    img = Image.new("RGBA", (24,24), self.bgcolor)
    alpha_over(img, side, (0,12 - delta), side)
    alpha_over(img, otherside, (12,12 - delta), otherside)
    alpha_over(img, top, (0,6 - delta), top)

    return img

# echelle en pin
@material(blockid=1150, data=[2, 3, 4, 5], transparent=True)
def ladderPine(self, blockid, data):

    # first rotations
    if self.rotation == 1:
        if data == 2: data = 5
        elif data == 3: data = 4
        elif data == 4: data = 2
        elif data == 5: data = 3
    elif self.rotation == 2:
        if data == 2: data = 3
        elif data == 3: data = 2
        elif data == 4: data = 5
        elif data == 5: data = 4
    elif self.rotation == 3:
        if data == 2: data = 4
        elif data == 3: data = 5
        elif data == 4: data = 3
        elif data == 5: data = 2

    img = Image.new("RGBA", (24,24), self.bgcolor)
    raw_texture = self.load_image_texture("assets/minecraft/textures/blocks/ladderpine.png")

    if data == 5:
        # normally this ladder would be obsured by the block it's attached to
        # but since ladders can apparently be placed on transparent blocks, we
        # have to render this thing anyway.  same for data == 2
        tex = self.transform_image_side(raw_texture)
        alpha_over(img, tex, (0,6), tex)
        return img
    if data == 2:
        tex = self.transform_image_side(raw_texture).transpose(Image.FLIP_LEFT_RIGHT)
        alpha_over(img, tex, (12,6), tex)
        return img
    if data == 3:
        tex = self.transform_image_side(raw_texture).transpose(Image.FLIP_LEFT_RIGHT)
        alpha_over(img, tex, (0,0), tex)
        return img
    if data == 4:
        tex = self.transform_image_side(raw_texture)
        alpha_over(img, tex, (12,0), tex)
        return img

# echelle en bouleau
@material(blockid=1151, data=[2, 3, 4, 5], transparent=True)
def ladderPine(self, blockid, data):

    # first rotations
    if self.rotation == 1:
        if data == 2: data = 5
        elif data == 3: data = 4
        elif data == 4: data = 2
        elif data == 5: data = 3
    elif self.rotation == 2:
        if data == 2: data = 3
        elif data == 3: data = 2
        elif data == 4: data = 5
        elif data == 5: data = 4
    elif self.rotation == 3:
        if data == 2: data = 4
        elif data == 3: data = 5
        elif data == 4: data = 3
        elif data == 5: data = 2

    img = Image.new("RGBA", (24,24), self.bgcolor)
    raw_texture = self.load_image_texture("assets/minecraft/textures/blocks/ladderbirch.png")

    if data == 5:
        # normally this ladder would be obsured by the block it's attached to
        # but since ladders can apparently be placed on transparent blocks, we
        # have to render this thing anyway.  same for data == 2
        tex = self.transform_image_side(raw_texture)
        alpha_over(img, tex, (0,6), tex)
        return img
    if data == 2:
        tex = self.transform_image_side(raw_texture).transpose(Image.FLIP_LEFT_RIGHT)
        alpha_over(img, tex, (12,6), tex)
        return img
    if data == 3:
        tex = self.transform_image_side(raw_texture).transpose(Image.FLIP_LEFT_RIGHT)
        alpha_over(img, tex, (0,0), tex)
        return img
    if data == 4:
        tex = self.transform_image_side(raw_texture)
        alpha_over(img, tex, (12,0), tex)
        return img

# echelle en bois de jungle
@material(blockid=1152, data=[2, 3, 4, 5], transparent=True)
def ladderPine(self, blockid, data):

    # first rotations
    if self.rotation == 1:
        if data == 2: data = 5
        elif data == 3: data = 4
        elif data == 4: data = 2
        elif data == 5: data = 3
    elif self.rotation == 2:
        if data == 2: data = 3
        elif data == 3: data = 2
        elif data == 4: data = 5
        elif data == 5: data = 4
    elif self.rotation == 3:
        if data == 2: data = 4
        elif data == 3: data = 5
        elif data == 4: data = 3
        elif data == 5: data = 2

    img = Image.new("RGBA", (24,24), self.bgcolor)
    raw_texture = self.load_image_texture("assets/minecraft/textures/blocks/ladderjungle.png")

    if data == 5:
        # normally this ladder would be obsured by the block it's attached to
        # but since ladders can apparently be placed on transparent blocks, we
        # have to render this thing anyway.  same for data == 2
        tex = self.transform_image_side(raw_texture)
        alpha_over(img, tex, (0,6), tex)
        return img
    if data == 2:
        tex = self.transform_image_side(raw_texture).transpose(Image.FLIP_LEFT_RIGHT)
        alpha_over(img, tex, (12,6), tex)
        return img
    if data == 3:
        tex = self.transform_image_side(raw_texture).transpose(Image.FLIP_LEFT_RIGHT)
        alpha_over(img, tex, (0,0), tex)
        return img
    if data == 4:
        tex = self.transform_image_side(raw_texture)
        alpha_over(img, tex, (12,0), tex)
        return img

# bougie1 (petite)
@material(blockid=1172, data=range(16), transparent=True)
def bougie1(self, blockid, data):
    tex = self.load_image_texture("assets/minecraft/textures/blocks/bougie-1-1.png")
    return self.build_sprite(tex)

# bougie2
@material(blockid=1173, data=range(16), transparent=True)
def bougie2(self, blockid, data):
    tex = self.load_image_texture("assets/minecraft/textures/blocks/bougie-2-1.png")
    return self.build_sprite(tex)

# bougie3 (grande)
@material(blockid=1174, data=range(16), transparent=True)
def bougie3(self, blockid, data):
    tex = self.load_image_texture("assets/minecraft/textures/blocks/bougie-3-1.png")
    return self.build_sprite(tex)

# bougie4 (petite + coupelle)
@material(blockid=1175, data=range(16), transparent=True)
def bougie4(self, blockid, data):
    tex = self.load_image_texture("assets/minecraft/textures/blocks/bougie-1-2.png")
    return self.build_sprite(tex)

# bougie5
@material(blockid=1176, data=range(16), transparent=True)
def bougie5(self, blockid, data):
    tex = self.load_image_texture("assets/minecraft/textures/blocks/bougie-2-2.png")
    return self.build_sprite(tex)

# bougie6 (grande + coupelle)
@material(blockid=1177, data=range(16), transparent=True)
def bougie6(self, blockid, data):
    tex = self.load_image_texture("assets/minecraft/textures/blocks/bougie-3-2.png")
    return self.build_sprite(tex)

# double slabs and slabs
@material(blockid=[1229], data=range(16), transparent=True, solid=True)
def slabs(self, blockid, data):
    wood_type = data & 7
    # choose textures
    top = self.load_image_texture("assets/minecraft/textures/blocks/tree_top.png")
    if wood_type == 0: # normal
        side = self.load_image_texture("assets/minecraft/textures/blocks/tree_side.png")
    elif wood_type == 1: # birch
        side = self.load_image_texture("assets/minecraft/textures/blocks/oak-side.png")
    elif wood_type == 2: # pine
        side = self.load_image_texture("assets/minecraft/textures/blocks/birch-side.png")
    elif wood_type == 3: # jungle wood
        side = self.load_image_texture("assets/minecraft/textures/blocks/jungle-side.png")
    else:
        return None
    # cut the side texture in half
    mask = side.crop((0,8,16,16))
    side = Image.new(side.mode, side.size, self.bgcolor)
    alpha_over(side, mask,(0,0,16,8), mask)

    # plain slab
    top = self.transform_image_top(top)
    side = self.transform_image_side(side)
    otherside = side.transpose(Image.FLIP_LEFT_RIGHT)

    sidealpha = side.split()[3]
    side = ImageEnhance.Brightness(side).enhance(0.9)
    side.putalpha(sidealpha)
    othersidealpha = otherside.split()[3]
    otherside = ImageEnhance.Brightness(otherside).enhance(0.8)
    otherside.putalpha(othersidealpha)

    # upside down slab
    delta = 0
    if data & 8 == 8:
        delta = 6

    img = Image.new("RGBA", (24,24), self.bgcolor)
    alpha_over(img, side, (0,12 - delta), side)
    alpha_over(img, otherside, (12,12 - delta), otherside)
    alpha_over(img, top, (0,6 - delta), top)

    return img


# block algue
@material(blockid=1222, data=range(4), solid=True)
def woodWithAlgue(self, blockid, data):
    if (data) == 0:
        texture = self.load_image_texture("assets/minecraft/textures/blocks/spruce-recouvert-algue.png")
    if (data) == 1:
        texture = self.load_image_texture("assets/minecraft/textures/blocks/pine-recouvert-algue.png")
    if (data) == 2:
        texture = self.load_image_texture("assets/minecraft/textures/blocks/birch-recouvert-algue.png")
    if (data) == 3:
        texture = self.load_image_texture("assets/minecraft/textures/blocks/jungle-recouvert-algue.png")
    return self.build_block(texture, texture)



@material(blockid=[1223], data=range(16), solid=True)
def wood(self, blockid, data):
    # extract orientation and wood type frorm data bits
    wood_type = data & 3
    wood_orientation = data & 12
    if self.rotation == 1:
        if wood_orientation == 4: wood_orientation = 8
        elif wood_orientation == 8: wood_orientation = 4
    elif self.rotation == 3:
        if wood_orientation == 4: wood_orientation = 8
        elif wood_orientation == 8: wood_orientation = 4

    if wood_type == 0: # normal
        top = self.load_image_texture("assets/minecraft/textures/blocks/log_oak_top.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/spruce-tronc-recouvert-algue.png")
    if wood_type == 1: # spruce
        top = self.load_image_texture("assets/minecraft/textures/blocks/log_spruce_top.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/pine-tronc-recouvert-algue.png")
    if wood_type == 2: # birch
        top = self.load_image_texture("assets/minecraft/textures/blocks/log_birch_top.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/birch-tronc-recouvert-algue.png")
    if wood_type == 3: # jungle wood
        top = self.load_image_texture("assets/minecraft/textures/blocks/log_jungle_top.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/jungle-tronc-recouvert-algue.png")
    # choose orientation and paste textures
    if wood_orientation == 0:
        return self.build_block(top, side)
    elif wood_orientation == 4: # east-west orientation
        return self.build_full_block(side.rotate(90), None, None, top, side.rotate(90))
    elif wood_orientation == 8: # north-south orientation
        return self.build_full_block(side, None, None, side.rotate(270), top)
    elif wood_orientation == 12: # full block
        return self.build_block(side, side)

# tronc couche algue
@material(blockid=1224, data=range(16), solid=True)
def troncCouche(self, blockid, data):
    orient = data & 0x8;
    woodtype = data & 0x7;
    if woodtype == 0:
        specialside = self.load_image_texture("assets/minecraft/textures/blocks/tree_side.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/spruce-tronc-recouvert-algue.png")
    elif woodtype == 1:
        specialside = self.load_image_texture("assets/minecraft/textures/blocks/oak-side.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/pine-tronc-recouvert-algue.png")
    elif woodtype == 2:
        specialside = self.load_image_texture("assets/minecraft/textures/blocks/birch-side.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/birch-tronc-recouvert-algue.png")
    else:
        specialside = self.load_image_texture("assets/minecraft/textures/blocks/jungle-side.png")
        side = self.load_image_texture("assets/minecraft/textures/blocks/jungle-tronc-recouvert-algue.png")

    if orient == 0:
      return self.build_full_block(side, None, None, specialside, self.load_image_texture("assets/minecraft/textures/blocks/tree_top.png"))
    else:
      return self.build_full_block(specialside, None, None, self.load_image_texture("assets/minecraft/textures/blocks/tree_top.png"), specialside)


# block stone algue
@material(blockid=1225, data=range(2), solid=True)
def woodWithAlgue(self, blockid, data):
    if (data) == 0:
        texture = self.load_image_texture("assets/minecraft/textures/blocks/stonebrick-recouvert-algue.png")
    if (data) == 1:
        texture = self.load_image_texture("assets/minecraft/textures/blocks/stone-recouvert-algue.png")
    return self.build_block(texture, texture)


block(blockid=1226, top_image="assets/minecraft/textures/blocks/glaise-recouvert-algue.png")
block(blockid=1227, top_image="assets/minecraft/textures/blocks/gravier-recouvert-algue.png")
block(blockid=1228, top_image="assets/minecraft/textures/blocks/neige-recouvert-algue.png")

# Ne fonctionne pas pour le moment.
@material(blockid=[1230], data=range(16), solid=True)
def doubleSlab(self, blockid, data):
    top = self.load_image_texture("assets/minecraft/textures/blocks/marbre-noir-halfstep-top.png")
    side = self.load_image_texture("assets/minecraft/textures/blocks/marbre-noir-halfstep-side.png")

    return self.build_block(top, side)

@material(blockid=[1231], data=range(16), solid=True)
def fullWood(self, blockid, data):
    # extract orientation and wood type frorm data bits
    wood_type = data & 3
    wood_orientation = data & 12
    if self.rotation == 1:
        if wood_orientation == 4: wood_orientation = 8
        elif wood_orientation == 8: wood_orientation = 4
    elif self.rotation == 3:
        if wood_orientation == 4: wood_orientation = 8
        elif wood_orientation == 8: wood_orientation = 4

    if wood_type == 0: # normal
        top = side = self.load_image_texture("assets/minecraft/textures/blocks/log_oak.png")
    if wood_type == 1: # spruce
        top = side = self.load_image_texture("assets/minecraft/textures/blocks/log_spruce.png")
    if wood_type == 2: # birch
        top = side = self.load_image_texture("assets/minecraft/textures/blocks/log_birch.png")
    if wood_type == 3: # jungle wood
        top = side = self.load_image_texture("assets/minecraft/textures/blocks/log_jungle.png")

    # choose orientation and paste textures
    if wood_orientation == 0:
        return self.build_block(top, side)
    elif wood_orientation == 4: # east-west orientation
        return self.build_full_block(side.rotate(90), None, None, top, side.rotate(90))
    elif wood_orientation == 8: # north-south orientation
        return self.build_full_block(side, None, None, side.rotate(270), top)
    elif wood_orientation == 12: # full block
        return self.build_block(side, side)

# forestGround
@material(blockid=[1232,1233,1234,1235,1236,1237,1238,1239,1240,1241,1242,1243,1244,1245,1246,1247,1248,1249,1250,1251,1252,1253,1254,1255,1256,1257],  data=range(16), transparent=True, solid=False)
def feuilles(self, blockid, data):

    rotation = data >> 1;
    texture = None
    data = data & 1;

    if blockid == 1232:
        if (data) == 0:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/grass-variation-1.png")
        if (data) == 1:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/grass-variation-2.png")
    if blockid == 1233:
        if (data) == 0:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/grass-variation-3.png")
        if (data) == 1:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/grass-variation-4.png")
    if blockid == 1234:
        if (data) == 0:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/grass-variation-5.png")
        if (data) == 1:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/grass-variation-6.png")
    if blockid == 1235:
        if (data) == 0:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/grass-variation-7.png")
        if (data) == 1:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/grass-variation-8.png")
    if blockid == 1236:
        if (data) == 0:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/stone-variation-1.png")
        if (data) == 1:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/stone-variation-2.png")
    if blockid == 1237:
        if (data) == 0:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/stone-variation-3.png")
        if (data) == 1:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/stone-variation-4.png")
    if blockid == 1238:
        if (data) == 0:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/stone-variation-5.png")
        if (data) == 1:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/stone-variation-6.png")
    if blockid == 1239:
        if (data) == 0:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/stone-variation-7.png")
        if (data) == 1:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/stone-variation-8.png")
    if blockid == 1240:
        if (data) == 0:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/leaf-variation-1.png")
        if (data) == 1:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/leaf-variation-2.png")
    if blockid == 1241:
        if (data) == 0:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/leaf-variation-3.png")
        if (data) == 1:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/leaf-variation-4.png")
    if blockid == 1242:
        if (data) == 0:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/leaf-variation-5.png")
        if (data) == 1:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/leaf-variation-6.png")
    if blockid == 1243:
        if (data) == 0:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/leaf-variation-7.png")
        if (data) == 1:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/leaf-variation-8.png")
    if blockid == 1244:
        if (data) == 0:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/thorn-variation-1.png")
        if (data) == 1:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/thorn-variation-2.png")
    if blockid == 1245:
        if (data) == 0:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/thorn-variation-3.png")
        if (data) == 1:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/thorn-variation-4.png")
    if blockid == 1246:
        if (data) == 0:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/thorn-variation-5.png")
        if (data) == 1:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/thorn-variation-6.png")
    if blockid == 1247:
        if (data) == 0:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/thorn-variation-7.png")
        if (data) == 1:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/thorn-variation-8.png")
    if blockid == 1248:
        if (data) == 0:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/leaf-1.png")
        if (data) == 1:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/leaf-2.png")
    if blockid == 1249:
        if (data) == 0:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/leaf-3.png")
        if (data) == 1:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/leaf-4.png")
    if blockid == 1250:
        if (data) == 0:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/leaf-5.png")
        if (data) == 1:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/leaf-6.png")
    if blockid == 1251:
        if (data) == 0:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/leaf-7.png")
        if (data) == 1:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/leaf-8.png")
    if blockid == 1252:
        if (data) == 0:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/leaf-9.png")
        if (data) == 1:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/leaf-10.png")
    if blockid == 1253:
        if (data) == 0:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/leaf-11.png")
        if (data) == 1:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/leaf-12.png")
    if blockid == 1254:
        if (data) == 0:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/mossy-1.png")
        if (data) == 1:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/mossy-2.png")
    if blockid == 1255:
        if (data) == 0:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/mossy-3.png")
        if (data) == 1:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/mossy-4.png")
    if blockid == 1256:
        if (data) == 0:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/leaf-13.png")
        if (data) == 1:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/leaf-14.png")
    if blockid == 1257:
        if (data) == 0:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/leaf-15.png")
        if (data) == 1:
            texture = self.load_image_texture("assets/minecraft/textures/blocks/leaf-16.png")

    top = s = w = n = e = None

    data = 0;
    if rotation == 0:
      top = texture
    elif rotation == 1:
      top = texture.copy().rotate(90)
    elif rotation == 2:
      top = texture.copy().rotate(180)
    elif rotation == 3:
      top = texture.copy().rotate(270)
    elif rotation == 4:
        data = 1
    elif rotation == 5:
        data = 4
    elif rotation == 6:
        data = 8
    elif rotation == 7:
        data = 2

    # rotate the data by bitwise shift
    shifts = 0
    if self.rotation == 1:
        shifts = 1
    elif self.rotation == 2:
        shifts = 2
    elif self.rotation == 3:
        shifts = 3

    for i in range(shifts):
        data = data * 2
        if data & 16:
            data = (data - 16) | 1

    if data & 1: # south
        s = texture
    if data & 2: # west
        w = texture
    if data & 4: # north
        n = texture
    if data & 8: # east
        e = texture

    if top != None:
        top = self.transform_image_top(top)
        img = Image.new("RGBA", (24,24), self.bgcolor)
        alpha_over(img, top, (0,12), top)
    else:
        img = self.build_full_block(None, n, e, w, s)

    return img

#Verre coloré en cul-de-bouteille
@material(blockid=1359, data=range(16), solid=True)
def verreColoreCrossed(self, blockid, data):
    t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_white.png")

    if (data) == 0:
            t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_orange.png")
    if (data) == 1:
            t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_white.png")
    if (data) == 2:
            t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_red.png")
    if (data) == 3:
            t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_black.png")
    if (data) == 4:
            t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_green.png")
    if (data) == 5:
            t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_brown.png")
    if (data) == 6:
            t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_blue.png")
    if (data) == 7:
            t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_purple.png")
    if (data) == 8:
            t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_cyan.png")
    if (data) == 9:
            t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_silver.png")
    if (data) == 10:
            t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_gray.png")
    if (data) == 11:
            t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_pink.png")
    if (data) == 12:
            t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_lime.png")
    if (data) == 13:
            t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_yellow.png")
    if (data) == 14:
            t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_light_blue.png")
    if (data) == 15:
            t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_magenta.png")

    half_block_u = t.copy() # up, down, left, right
    half_block_d = t.copy()
    half_block_l = t.copy()
    half_block_r = t.copy()

    # generate needed geometries
    ImageDraw.Draw(half_block_u).rectangle((0,8,15,15),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(half_block_d).rectangle((0,0,15,6),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(half_block_l).rectangle((8,0,15,15),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(half_block_r).rectangle((0,0,7,15),outline=(0,0,0,0),fill=(0,0,0,0))

    return self.build_block(t, t)

#Vitre colorées en cul-de-bouteille
@material(blockid=1360, data=range(256), transparent=True, nospawn=True)
def panes(self, blockid, data):
    blockdata = data & 0xf;
    if (blockdata) == 0:
      t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_orange.png")
    elif (blockdata) == 1:
      t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_white.png")
    elif (blockdata) == 2:
      t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_red.png")
    elif (blockdata) == 3:
      t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_black.png")
    elif (blockdata) == 4:
      t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_green.png")
    elif (blockdata) == 5:
      t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_brown.png")
    elif (blockdata) == 6:
      t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_blue.png")
    elif (blockdata) == 7:
      t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_purple.png")
    elif (blockdata) == 8:
      t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_cyan.png")
    elif (blockdata) == 9:
      t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_silver.png")
    elif (blockdata) == 10:
      t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_gray.png")
    elif (blockdata) == 11:
      t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_pink.png")
    elif (blockdata) == 12:
      t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_lime.png")
    elif (blockdata) == 13:
      t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_yellow.png")
    elif (blockdata) == 14:
      t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_light_blue.png")
    elif (blockdata) == 15:
      t = self.load_image_texture("assets/minecraft/textures/blocks/glass_cerc_magenta.png")

    left = t.copy()
    right = t.copy()

    # generate the four small pieces of the glass pane
    ImageDraw.Draw(right).rectangle((0,0,7,15),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(left).rectangle((8,0,15,15),outline=(0,0,0,0),fill=(0,0,0,0))

    up_left = self.transform_image_side(left)
    up_right = self.transform_image_side(right).transpose(Image.FLIP_TOP_BOTTOM)
    dw_right = self.transform_image_side(right)
    dw_left = self.transform_image_side(left).transpose(Image.FLIP_TOP_BOTTOM)

    # Create img to compose the texture
    img = Image.new("RGBA", (24,24), self.bgcolor)

    # +x axis points top right direction
    # +y axis points bottom right direction
    # First compose things in the back of the image,
    # then things in the front.

    # the lower 4 bits encode color, the upper 4 encode adjencies
    data = data >> 4

    if (data & 0b0001) == 1 or data == 0:
        alpha_over(img,up_left, (6,3),up_left)    # top left
    if (data & 0b1000) == 8 or data == 0:
        alpha_over(img,up_right, (6,3),up_right)  # top right
    if (data & 0b0010) == 2 or data == 0:
        alpha_over(img,dw_left, (6,3),dw_left)    # bottom left
    if (data & 0b0100) == 4 or data == 0:
        alpha_over(img,dw_right, (6,3),dw_right)  # bottom right

    return img

@material(blockid=range(1365,1388), data=range(16), solid=True)
def wood(self, blockid, data):
    # extract orientation and wood type frorm data bits
    wood_type = data & 3
    wood_orientation = data & 12
    if self.rotation == 1:
        if wood_orientation == 4: wood_orientation = 8
        elif wood_orientation == 8: wood_orientation = 4
    elif self.rotation == 3:
        if wood_orientation == 4: wood_orientation = 8
        elif wood_orientation == 8: wood_orientation = 4

    # choose textures
    if blockid <= 1377: # regular wood:
	variantId = blockid - 1365;
        if wood_type == 0: # normal
            top = self.load_image_texture("assets/minecraft/textures/blocks/log_oak_top.png")
            side = self.load_image_texture("assets/minecraft/textures/blocks/carvedlog_oak_" + str(variantId) + ".png")
        if wood_type == 1: # spruce
            top = self.load_image_texture("assets/minecraft/textures/blocks/log_spruce_top.png")
            side = self.load_image_texture("assets/minecraft/textures/blocks/carvedlog_spruce_" + str(variantId) + ".png")
        if wood_type == 2: # birch
            top = self.load_image_texture("assets/minecraft/textures/blocks/log_birch_top.png")
            side = self.load_image_texture("assets/minecraft/textures/blocks/carvedlog_birch_" + str(variantId) + ".png")
        if wood_type == 3: # jungle wood
            top = self.load_image_texture("assets/minecraft/textures/blocks/log_jungle_top.png")
            side = self.load_image_texture("assets/minecraft/textures/blocks/carvedlog_jungle_" + str(variantId) + ".png")
    else: # acacia/dark wood:
	variantId = blockid - 1377;
        if wood_type == 0: # acacia
            top = self.load_image_texture("assets/minecraft/textures/blocks/log_acacia_top.png")
            side = self.load_image_texture("assets/minecraft/textures/blocks/carvedlog_acacia_" + str(variantId) + ".png")
        elif wood_type == 1: # dark oak
            top = self.load_image_texture("assets/minecraft/textures/blocks/log_big_oak_top.png")
            side = self.load_image_texture("assets/minecraft/textures/blocks/carvedlog_dark_oak_" + str(variantId) + ".png")
        else:
            top = self.load_image_texture("assets/minecraft/textures/blocks/log_acacia_top.png")
            side = self.load_image_texture("assets/minecraft/textures/blocks/carvedlog_acacia_" + str(variantId) + ".png")

    # choose orientation and paste textures
    if wood_orientation == 0:
        return self.build_block(top, side)
    elif wood_orientation == 4: # east-west orientation
        return self.build_full_block(side.rotate(90), None, None, top, side.rotate(90))
    elif wood_orientation == 8: # north-south orientation
        return self.build_full_block(side, None, None, side.rotate(270), top)
    elif wood_orientation == 12: # full block
        return self.build_block(side.rotate(90), side)


#Structures de lation acier cuivre + grille d'acier
@material(blockid=1092, data=range(4), solid=True)
def verreColoreCrossed(self, blockid, data):
    t = self.load_image_texture("assets/minecraft/textures/blocks/structure-cuivre.png")

    if (data) == 0:
            t = self.load_image_texture("assets/minecraft/textures/blocks/structure-cuivre.png")
    if (data) == 1:
            t = self.load_image_texture("assets/minecraft/textures/blocks/structure-acier.png")
    if (data) == 2:
            t = self.load_image_texture("assets/minecraft/textures/grille-acier.png")
    if (data) == 3:
            t = self.load_image_texture("assets/minecraft/textures/blocks/structure-laiton.png")

    half_block_u = t.copy() # up, down, left, right
    half_block_d = t.copy()
    half_block_l = t.copy()
    half_block_r = t.copy()

    # generate needed geometries
    ImageDraw.Draw(half_block_u).rectangle((0,8,15,15),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(half_block_d).rectangle((0,0,15,6),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(half_block_l).rectangle((8,0,15,15),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(half_block_r).rectangle((0,0,7,15),outline=(0,0,0,0),fill=(0,0,0,0))

    return self.build_block(t, t)


#bloc de lation + variantes
@material(blockid=1261, data=range(6), solid=True)
def verreColoreCrossed(self, blockid, data):
    t = self.load_image_texture("assets/minecraft/textures/blocks/bloc-laiton.png")

    if (data) == 0:
            t = self.load_image_texture("assets/minecraft/textures/blocks/bloc-laiton.png")
    if (data) == 1:
            t = self.load_image_texture("assets/minecraft/textures/blocks/brique-laiton.png")
    if (data) == 2:
            t = self.load_image_texture("assets/minecraft/textures/bloc-laiton-oxyde.png")
    if (data) == 3:
            t = self.load_image_texture("assets/minecraft/textures/blocks/brique-laiton-oxyde.png")
    if (data) == 4:
            t = self.load_image_texture("assets/minecraft/textures/blocks/bloc-laiton-tres-oxyde.png")
    if (data) == 5:
            t = self.load_image_texture("assets/minecraft/textures/blocks/brique-laiton-tres-oxyde.png")

    half_block_u = t.copy() # up, down, left, right
    half_block_d = t.copy()
    half_block_l = t.copy()
    half_block_r = t.copy()

    # generate needed geometries
    ImageDraw.Draw(half_block_u).rectangle((0,8,15,15),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(half_block_d).rectangle((0,0,15,6),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(half_block_l).rectangle((8,0,15,15),outline=(0,0,0,0),fill=(0,0,0,0))
    ImageDraw.Draw(half_block_r).rectangle((0,0,7,15),outline=(0,0,0,0),fill=(0,0,0,0))

    return self.build_block(t, t)

@material(blockid=[1264,1265,1266,1267,1268,1269], data=range(128), transparent=True, solid=True, nospawn=True)
def stairs(self, blockid, data):
    # preserve the upside-down bit
    upside_down = data & 0x4

    # find solid quarters within the top or bottom half of the block
    #                   NW           NE           SE           SW
    quarters = [data & 0x8, data & 0x10, data & 0x20, data & 0x40]

    # rotate the quarters so we can pretend northdirection is always upper-left
    numpy.roll(quarters, [0,1,3,2][self.rotation])
    nw,ne,se,sw = quarters

    if blockid == 1264: # brique laiton
        texture = self.load_image_texture("assets/minecraft/textures/blocks/brique-laiton.png").copy()
    elif blockid == 1265: # brique laiton oxydé
        texture = self.load_image_texture("assets/minecraft/textures/blocks/brique-laiton-oxyde.png").copy()
    elif blockid == 1266: # brique laiton très oxydé
        texture = self.load_image_texture("assets/minecraft/textures/blocks/brique-laiton-tres-oxyde.png").copy()
    elif blockid == 1267: # bloc laiton
        texture = self.load_image_texture("assets/minecraft/textures/blocks/bloc-laiton.png").copy()
    elif blockid == 1268: # bloc laiton oxydé
        texture = self.load_image_texture("assets/minecraft/textures/blocks/nether_brick.png").copy()
    elif blockid == 1269: # bloc laiton  très oxydé
        texture = self.load_image_texture("assets/minecraft/textures/blocks/bloc-laiton-tres-oxyde.png").copy()


    outside_l = texture.copy()
    outside_r = texture.copy()
    inside_l = texture.copy()
    inside_r = texture.copy()
    
    slab_top = texture.copy()

    push = 8 if upside_down else 0

    def rect(tex,coords):
        ImageDraw.Draw(tex).rectangle(coords,outline=(0,0,0,0),fill=(0,0,0,0))

    # cut out top or bottom half from inner surfaces
    rect(inside_l, (0,8-push,15,15-push))
    rect(inside_r, (0,8-push,15,15-push))

    # cut out missing or obstructed quarters from each surface
    if not nw:
        rect(outside_l, (0,push,7,7+push))
        rect(texture, (0,0,7,7))
    if not nw or sw:
        rect(inside_r, (8,push,15,7+push)) # will be flipped
    if not ne:
        rect(texture, (8,0,15,7))
    if not ne or nw:
        rect(inside_l, (0,push,7,7+push))
    if not ne or se:
        rect(inside_r, (0,push,7,7+push)) # will be flipped
    if not se:
        rect(outside_r, (0,push,7,7+push)) # will be flipped
        rect(texture, (8,8,15,15))
    if not se or sw:
        rect(inside_l, (8,push,15,7+push))
    if not sw:
        rect(outside_l, (8,push,15,7+push))
        rect(outside_r, (8,push,15,7+push)) # will be flipped
        rect(texture, (0,8,7,15))

    img = Image.new("RGBA", (24,24), self.bgcolor)

    if upside_down:
        # top should have no cut-outs after all
        texture = slab_top
    else:
        # render the slab-level surface
        slab_top = self.transform_image_top(slab_top)
        alpha_over(img, slab_top, (0,6))

    # render inner left surface
    inside_l = self.transform_image_side(inside_l)
    # Darken the vertical part of the second step
    sidealpha = inside_l.split()[3]
    # darken it a bit more than usual, looks better
    inside_l = ImageEnhance.Brightness(inside_l).enhance(0.8)
    inside_l.putalpha(sidealpha)
    alpha_over(img, inside_l, (6,3))

    # render inner right surface
    inside_r = self.transform_image_side(inside_r).transpose(Image.FLIP_LEFT_RIGHT)
    # Darken the vertical part of the second step
    sidealpha = inside_r.split()[3]
    # darken it a bit more than usual, looks better
    inside_r = ImageEnhance.Brightness(inside_r).enhance(0.7)
    inside_r.putalpha(sidealpha)
    alpha_over(img, inside_r, (6,3))

    # render outer surfaces
    alpha_over(img, self.build_full_block(texture, None, None, outside_l, outside_r))

    return img